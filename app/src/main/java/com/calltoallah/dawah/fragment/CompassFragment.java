package com.calltoallah.dawah.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Compass;
import com.calltoallah.dawah.comman.LocationTrack;

public class CompassFragment extends BaseFragment {

    private ImageView image_compass, compassSelected;
    private Compass compass;
    private float currentAzimuth;

    private static final int REQUEST_CHECK_SETTINGS = 100;
    private TextView txtKaabaDirection, txtCurrentLocation;
    private String latitude, longitude;
    private double kaabaDirectionResult;
    private LocationTrack mLocationTrack;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_compass, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);

        mLocationTrack = new LocationTrack(getActivity());
        if (mLocationTrack.canGetLocation()) {
            double longitude2 = 39.826206; // ka'bah Position https://www.latlong.net/place/kaaba-mecca-saudi-arabia-12639.html
            double longitude1 = mLocationTrack.getLongitude();
            double latitude2 = Math.toRadians(21.422487); // ka'bah Position https://www.latlong.net/place/kaaba-mecca-saudi-arabia-12639.html
            double latitude1 = Math.toRadians(mLocationTrack.getLatitude());
            double longDiff = Math.toRadians(longitude2 - longitude1);
            double y = Math.sin(longDiff) * Math.cos(latitude2);
            double x = Math.cos(latitude1) * Math.sin(latitude2) - Math.sin(latitude1) * Math.cos(latitude2) * Math.cos(longDiff);
            kaabaDirectionResult = (Math.toDegrees(Math.atan2(y, x)) + 360) % 360;
            txtKaabaDirection.setText(getResources().getString(R.string.qibla_direction) + " " + kaabaDirectionResult + "\n" + getResources().getString(R.string.degree_from_north));

            latitude = String.valueOf(mLocationTrack.getLatitude());
            longitude = String.valueOf(mLocationTrack.getLongitude());
//            city = addresses.get(0).getLocality();
//            state = addresses.get(0).getAdminArea();
//            country = addresses.get(0).getCountryName();
//            txtCurrentLocation.setText(city + ", " + state + ", " + country);

        } else {
            mLocationTrack.showSettingsAlert();
        }

        setData();
    }

    private void setData() {
        setupCompass();
    }

    private void setupCompass() {
        compass = new Compass(getActivity());
        if (compass != null) {
            compass.start();
        }

        compass.setListener(new Compass.CompassListener() {
            @Override
            public void onNewAzimuth(float azimuth) {
                adjustGambarDial(azimuth);
                adjustArrowQiblat(azimuth);
            }
        });
    }

    public void adjustGambarDial(float azimuth) {
        Animation an = new RotateAnimation(-currentAzimuth, -azimuth,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        currentAzimuth = (azimuth);
//        an.setDuration(500);
        an.setRepeatCount(0);
        an.setFillAfter(true);
        image_compass.startAnimation(an);
    }

    public void adjustArrowQiblat(float azimuth) {
        float kiblat_derajat = (float) kaabaDirectionResult;
        Animation an = new RotateAnimation(-(currentAzimuth) + kiblat_derajat, -azimuth,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        currentAzimuth = (azimuth);
//        an.setDuration(500);
        an.setRepeatCount(0);
        an.setFillAfter(true);
//        image_qibla.startAnimation(an);
//        if (kiblat_derajat > 0) {
//            image_qibla.setVisibility(View.VISIBLE);
//        } else {
//            image_qibla.setVisibility(INVISIBLE);
//            image_qibla.setVisibility(View.GONE);
//        }
    }

    private void Init(View v) {
        image_compass = (ImageView) v.findViewById(R.id.image_compass);
        compassSelected = (ImageView) v.findViewById(R.id.compassSelected);
        compassSelected.bringToFront();

        txtKaabaDirection = (TextView) v.findViewById(R.id.txtKaabaDirection);
        txtCurrentLocation = (TextView) v.findViewById(R.id.txtCurrentLocation);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

