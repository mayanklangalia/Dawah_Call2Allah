package com.calltoallah.dawah.fragment;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.MasjidFollowerAdapter;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.model.FollowerListModel;
import com.calltoallah.dawah.model.MasjidListModel;

import java.util.ArrayList;
import java.util.Locale;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class MasjidOwnershipFragment extends BaseFragment {

    private ArrayList<FollowerListModel> mListItems;
    private RecyclerView rcvAdminList;
    private Bundle bundle;
    private SearchView mSearchView;
    private int REQUEST_VOICE = 13;
    private MasjidFollowerAdapter masjidFollowerAdapter;
    private MasjidListModel masjidListModel;
    private ImageView adminImage;
    private TextView adminName, adminUsername;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_follower, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {

        bundle = getArguments();
        if (bundle != null) {
            masjidListModel = bundle.getParcelable(Constants.OTHER);
            mListItems = bundle.getParcelableArrayList(Constants.DATA);
        }

        Log.d("ADMIN_NAME: ", masjidListModel.getFull_name());
        adminName.setText(masjidListModel.getFull_name().trim());
        adminUsername.setText(masjidListModel.getUsername());
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.no_profile_image)
                .error(R.drawable.no_profile_image);

        Glide.with(getActivity())
                .load(API_DOMAIN + masjidListModel.getUser_avatar())
                .apply(options)
                .into(adminImage);

        if (mListItems.size() > 0) {

            ArrayList<FollowerListModel> mFilterList = new ArrayList<>();
            for (FollowerListModel followerListModel : mListItems) {
                if (!followerListModel.getUserId().equalsIgnoreCase(masjidListModel.getUserId())) {
                    mFilterList.add(followerListModel);
                }
            }

            masjidFollowerAdapter = new MasjidFollowerAdapter(activity, mFilterList, masjidListModel, 1);
            rcvAdminList.setAdapter(masjidFollowerAdapter);
        }

        mSearchView.setQueryRefinementEnabled(true);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (masjidFollowerAdapter != null) {
                    masjidFollowerAdapter.getFilter().filter(query);
                    hideKeyboard();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (masjidFollowerAdapter != null) {
                    masjidFollowerAdapter.getFilter().filter(query);
                }
                return false;
            }
        });

        ImageView clearButton = (ImageView) mSearchView.findViewById(R.id.search_close_btn);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchView.setQuery("", false);
                masjidFollowerAdapter.getFilter().filter("");
                mSearchView.clearFocus();
            }
        });

        ImageView voiceButton = (ImageView) mSearchView.findViewById(R.id.search_voice_btn);
        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askSpeechInput();
            }
        });
    }

    private void askSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Try saying something");
        try {
            startActivityForResult(intent, REQUEST_VOICE);
        } catch (ActivityNotFoundException a) {
            a.printStackTrace();
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void Init(View v) {
        SearchManager mSearchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) v.findViewById(R.id.searchView);

        adminImage = (ImageView) v.findViewById(R.id.adminImage);
        adminName = (TextView) v.findViewById(R.id.adminName);
        adminUsername = (TextView) v.findViewById(R.id.adminUsername);

        mSearchView.setSearchableInfo(mSearchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconified(false);
        mSearchView.clearFocus();

        rcvAdminList = (RecyclerView) v.findViewById(R.id.rcvAdminList);
        rcvAdminList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rcvAdminList.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

