package com.calltoallah.dawah.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.model.MasjidModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.calltoallah.dawah.qrcode_reader.BarcodeReaderFragment;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class BarcodeFragment extends BaseFragment implements BarcodeReaderFragment.BarcodeReaderListener {

    private static final String TAG = BarcodeFragment.class.getSimpleName();
    private BarcodeReaderFragment barcodeReader;
    private ImageView imgFlash, imgGallery, imgBack;
    private boolean isFlashOn = false;
    private BarcodeDetector detector;
    private Uri imageUri;
    private static final int GALLERY_REQUEST = 13;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        View view = inflater.inflate(R.layout.fragment_barcode, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        detector = new BarcodeDetector.Builder(getActivity())
                .setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE)
                .build();

        barcodeReader = (BarcodeReaderFragment) getChildFragmentManager().findFragmentById(R.id.barcode_fragment);
        barcodeReader.setListener(this);

        imgBack = (ImageView) view.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
                activity.removeFragment(fragment);
            }
        });

        imgFlash = (ImageView) view.findViewById(R.id.imgFlash);
        imgFlash.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (!isFlashOn) {
                    barcodeReader.setUseFlash(true);
                    imgFlash.setImageResource(R.drawable.ic_action_flash);
                    isFlashOn = true;
                } else {
                    barcodeReader.setUseFlash(false);
                    imgFlash.setImageResource(R.drawable.ic_action_flash_off);
                    isFlashOn = false;
                }
            }
        });

        imgGallery = (ImageView) view.findViewById(R.id.imgGallery);
        imgGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, GALLERY_REQUEST);
            }
        });
    }

    @Override
    public void onScanned(final Barcode barcode) {
        barcodeReader.playBeep();

        Log.d("barcode_result: ", "" + barcode.displayValue);
        if (barcode.displayValue.contains("http://www.calltoallah.com")) {
            Log.d("barcode: ", "Found! " + barcode.displayValue
                    .substring(barcode.displayValue.lastIndexOf("/"))
                    .replace("/", ""));

            String masjidID = barcode.displayValue
                    .substring(barcode.displayValue.lastIndexOf("/"))
                    .replace("/", "");
            getMasjidDetails(masjidID);

        } else {
            Log.d("barcode: ", "Not Found!");
        }
    }

    private void getMasjidDetails(String masjidID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getMasjidDetails(masjidID);
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    Log.d("MASJID_DETAIL: ", response.body().toString());

                    try {
                        Gson gson = new Gson();
                        Reader reader = new StringReader(response.body().toString());
                        final MasjidModel masjidModel = gson.fromJson(reader, MasjidModel.class);

                        if (masjidModel.getMasjidListModel().size() > 0) {
                            Bundle bundle = new Bundle();
                            bundle.putInt(Constants.MODE, 2);
                            bundle.putParcelable(Constants.DATA, masjidModel.getMasjidListModel().get(0));

                            MasjidDetailsFragment masjidDetailsFragment = new MasjidDetailsFragment();
                            masjidDetailsFragment.setArguments(bundle);
                            activity.loadFragment(masjidDetailsFragment);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {
//        String codes = "";
//        for (Barcode barcode : barcodes) {
//            codes += barcode.displayValue + ", ";
//        }
//
//        final String finalCodes = codes;
//        Toast.makeText(getActivity(), "Barcodes: " + finalCodes, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {
        Log.e(TAG, "onScanError: " + errorMessage);
    }

    @Override
    public void onCameraPermissionDenied() {
        Toast.makeText(getActivity(), "Camera permission denied!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            launchMediaScanIntent();

            if (data != null) {
                // Get the Image from data
                Uri selectedImage = data.getData();

                try {
                    Bitmap bitmap = decodeBitmapUri(getActivity(), selectedImage);
                    if (detector.isOperational() && bitmap != null) {
                        Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                        SparseArray<Barcode> barcodes = detector.detect(frame);
                        Barcode code = barcodes.valueAt(0);

                        String masjidID = code.displayValue.substring(code.displayValue.lastIndexOf("/"))
                                .replace("/", "")
                                .replace(".html", "");
                        getMasjidDetails(masjidID);

                    } else {
                        Toast.makeText(activity, "Detector initialisation failed!", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Toast.makeText(activity, "Barcode detection failed!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void launchMediaScanIntent() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(imageUri);
        activity.sendBroadcast(mediaScanIntent);
    }

    private Bitmap decodeBitmapUri(Context ctx, Uri uri) throws FileNotFoundException {
        int targetW = 600;
        int targetH = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        return BitmapFactory.decodeStream(ctx.getContentResolver()
                .openInputStream(uri), null, bmOptions);
    }
}