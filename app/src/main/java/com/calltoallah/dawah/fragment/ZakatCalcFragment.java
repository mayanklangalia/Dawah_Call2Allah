package com.calltoallah.dawah.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.calltoallah.dawah.DonationActivity;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.popup.PopupWindow;

public class ZakatCalcFragment extends BaseFragment {

    private TextView txtTotalAssets, txtZakatPayable;
    private EditText edtNisab, edtGoldAmount, edtSilverAmount,
            edtInHand, edtDeposited, edtGiven, edtBusiness,
            edtStock, edtBorrowed, edtWages, edtTaxes;
    private Button zakatSubmit, zakatCancel, btnDonationNow;
    private LinearLayout linearGold, linearSilver;
    private String goldValue = "", silverValue = "", inHandValue = "", givenValue = "", depositedValue = "", businessValue = "",
            stockValue = "", borrowedValue = "", wagesValue = "", taxesValue = "";
    private double tempAns = 0;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_zakat_calc, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {

        edtNisab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupWindow testPopupWindow = new PopupWindow(getActivity(), new int[]{R.id.txt2, R.id.txt3});
                testPopupWindow.showAsDropDown(edtNisab);

                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt1)).setVisibility(View.GONE);
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt4)).setVisibility(View.GONE);
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt2)).setText("GOLD");
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt3)).setText("SILVER");

                testPopupWindow.setOnItemClickListener(new PopupWindow.OnItemClickListener() {
                    @Override
                    public void OnItemClick(View v) {
                        switch (v.getId()) {
                            case R.id.txt2:
                                edtNisab.setText("Gold");
                                linearGold.setVisibility(View.VISIBLE);
                                linearSilver.setVisibility(View.GONE);
                                break;

                            case R.id.txt3:
                                edtNisab.setText("Silver");
                                linearGold.setVisibility(View.GONE);
                                linearSilver.setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                });
            }
        });

        zakatSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (linearGold.getVisibility() == View.VISIBLE) {
                    if (edtGoldAmount.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(activity, "Please enter " + getString(R.string.valueOfGold), Toast.LENGTH_SHORT).show();
                    } else if (edtInHand.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(activity, "Please enter " + getString(R.string.in_hand), Toast.LENGTH_SHORT).show();
                    } else {
                        goldValue = edtGoldAmount.getText().toString();
                        inHandValue = edtInHand.getText().toString();
                        silverValue = "0";

                        if (edtDeposited.getText().toString().equalsIgnoreCase("") ||
                                edtGiven.getText().toString().equalsIgnoreCase("") ||
                                edtBusiness.getText().toString().equalsIgnoreCase("") ||
                                edtStock.getText().toString().equalsIgnoreCase("") ||
                                edtBorrowed.getText().toString().equalsIgnoreCase("") ||
                                edtWages.getText().toString().equalsIgnoreCase("") ||
                                edtTaxes.getText().toString().equalsIgnoreCase("")) {

                            depositedValue = "0";
                            givenValue = "0";
                            businessValue = "0";
                            stockValue = "0";
                            borrowedValue = "0";
                            wagesValue = "0";
                            taxesValue = "0";

                        } else {
                            depositedValue = edtDeposited.getText().toString();
                            givenValue = edtGiven.getText().toString();
                            businessValue = edtBusiness.getText().toString();
                            stockValue = edtStock.getText().toString();
                            borrowedValue = edtBorrowed.getText().toString();
                            wagesValue = edtWages.getText().toString();
                            taxesValue = edtTaxes.getText().toString();
                        }

                        calculateZakat(goldValue, silverValue, inHandValue, depositedValue, givenValue, businessValue, stockValue, borrowedValue, wagesValue, taxesValue);
                    }
                }

                if (linearSilver.getVisibility() == View.VISIBLE) {
                    if (edtSilverAmount.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(activity, "Please enter " + getString(R.string.valueOfSilver), Toast.LENGTH_SHORT).show();
                    } else if (edtInHand.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(activity, "Please enter " + getString(R.string.in_hand), Toast.LENGTH_SHORT).show();
                    } else {
                        silverValue = edtSilverAmount.getText().toString();
                        inHandValue = edtInHand.getText().toString();
                        goldValue = "0";

                        if (edtDeposited.getText().toString().equalsIgnoreCase("") ||
                                edtGiven.getText().toString().equalsIgnoreCase("") ||
                                edtBusiness.getText().toString().equalsIgnoreCase("") ||
                                edtStock.getText().toString().equalsIgnoreCase("") ||
                                edtBorrowed.getText().toString().equalsIgnoreCase("") ||
                                edtWages.getText().toString().equalsIgnoreCase("") ||
                                edtTaxes.getText().toString().equalsIgnoreCase("")) {

                            depositedValue = "0";
                            givenValue = "0";
                            businessValue = "0";
                            stockValue = "0";
                            borrowedValue = "0";
                            wagesValue = "0";
                            taxesValue = "0";

                        } else {
                            depositedValue = edtDeposited.getText().toString();
                            givenValue = edtGiven.getText().toString();
                            businessValue = edtBusiness.getText().toString();
                            stockValue = edtStock.getText().toString();
                            borrowedValue = edtBorrowed.getText().toString();
                            wagesValue = edtWages.getText().toString();
                            taxesValue = edtTaxes.getText().toString();
                        }

                        calculateZakat(goldValue, silverValue, inHandValue, depositedValue, givenValue, businessValue, stockValue, borrowedValue, wagesValue, taxesValue);
                    }
                }
            }
        });

        zakatCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtGoldAmount.setText("");
                edtSilverAmount.setText("");
                edtInHand.setText("");
                edtDeposited.setText("");
                edtGiven.setText("");
                edtBusiness.setText("");
                edtStock.setText("");
                edtBorrowed.setText("");
                edtWages.setText("");
                edtTaxes.setText("");

                edtNisab.setText("Gold");
                txtTotalAssets.setText(getString(R.string.rupees));
                txtZakatPayable.setText(getString(R.string.rupees));
            }
        });

        btnDonationNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (UserPreferences.loadUser(getActivity()).getVerify_status().equalsIgnoreCase("1")) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.MODE, 2);
                    bundle.putDouble(Constants.DATA, tempAns);

                    Intent intent = new Intent(getActivity(), DonationActivity.class);
                    intent.putExtras(bundle);
                    activity.startActivity(intent);

                } else {
                    activity.loadFragment(new UserVerificationFragment());
                }
            }
        });
    }

    private void calculateZakat(String goldValue, String silverValue, String inHandValue, String depositedValue, String givenValue, String businessValue,
                                String stockValue, String borrowedValue, String wagesValue, String taxesValue) {
        float subTotalAmount = 0;
        subTotalAmount += Float.parseFloat(goldValue);
        subTotalAmount += Float.parseFloat(silverValue);
        subTotalAmount += Float.parseFloat(inHandValue);
        subTotalAmount += Float.parseFloat(depositedValue);
        subTotalAmount += Float.parseFloat(givenValue);
        subTotalAmount += Float.parseFloat(businessValue);
        subTotalAmount -= Float.parseFloat(stockValue);
        subTotalAmount -= Float.parseFloat(borrowedValue);
        subTotalAmount -= Float.parseFloat(wagesValue);
        subTotalAmount -= Float.parseFloat(taxesValue);

        float newGoldValue = Float.parseFloat(goldValue);
        float newSilverValue = Float.parseFloat(silverValue);

        if (edtNisab.getText().toString().equalsIgnoreCase("Silver")) {
            if (subTotalAmount >= newGoldValue) {
                tempAns = (subTotalAmount * 2.5) / 100;
                txtTotalAssets.setText("₹ " + String.format("%s", subTotalAmount));
                txtZakatPayable.setText("₹ " + String.format("%s", tempAns));
            } else {
                txtTotalAssets.setText(getString(R.string.rupees));
                txtZakatPayable.setText(getString(R.string.rupees));
            }

        } else if (edtNisab.getText().toString().equalsIgnoreCase("Gold")) {
            if (subTotalAmount >= newSilverValue) {
                tempAns = (subTotalAmount * 2.5) / 100;
                txtTotalAssets.setText("₹ " + String.format("%s", subTotalAmount));
                txtZakatPayable.setText("₹ " + String.format("%s", tempAns));
            } else {
                txtTotalAssets.setText(getString(R.string.rupees));
                txtZakatPayable.setText(getString(R.string.rupees));
            }
        }
    }

    private void Init(View v) {
        edtNisab = (EditText) v.findViewById(R.id.edtNisab);
        edtGoldAmount = (EditText) v.findViewById(R.id.edtGoldAmount);
        edtSilverAmount = (EditText) v.findViewById(R.id.edtSilverAmount);
        edtInHand = (EditText) v.findViewById(R.id.edtInHand);
        edtDeposited = (EditText) v.findViewById(R.id.edtDeposited);
        edtGiven = (EditText) v.findViewById(R.id.edtGiven);
        edtBusiness = (EditText) v.findViewById(R.id.edtBusiness);
        edtStock = (EditText) v.findViewById(R.id.edtStock);
        edtBorrowed = (EditText) v.findViewById(R.id.edtBorrowed);
        edtWages = (EditText) v.findViewById(R.id.edtWages);
        edtTaxes = (EditText) v.findViewById(R.id.edtTaxes);

        zakatSubmit = (Button) v.findViewById(R.id.zakatSubmit);
        zakatCancel = (Button) v.findViewById(R.id.zakatCancel);
        btnDonationNow = (Button) v.findViewById(R.id.btnDonationNow);

        txtTotalAssets = (TextView) v.findViewById(R.id.txtTotalAssets);
        txtZakatPayable = (TextView) v.findViewById(R.id.txtZakatPayable);

        linearGold = (LinearLayout) v.findViewById(R.id.linearGold);
        linearSilver = (LinearLayout) v.findViewById(R.id.linearSilver);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

