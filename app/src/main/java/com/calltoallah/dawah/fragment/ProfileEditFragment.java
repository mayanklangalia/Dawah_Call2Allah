package com.calltoallah.dawah.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.BuildConfig;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.CircularImageView;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.ProfileModel;
import com.calltoallah.dawah.model.ProfileUserInfo;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Logger;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class ProfileEditFragment extends BaseFragment {

    private TextView txtVerifiedUserID, txtVerifiedName, txtVerifiedUsername, txtVerifiedMobile, txtVerifiedEmail, txtVerifiedChangePassword;
    private ImageView imgUpload, imgUsername, imgMobile, imgEmail, imgChangePassword;
    private CircularImageView imgVerifiedImage;
    private ProgressbarManager progressbarManager;
    private boolean responseFailed = false;

    private static final String IMAGE_DIRECTORY_NAME = "Dawah";
    private static final int REQUEST_TAKE_PHOTO = 02;
    private static final int REQUEST_PICK_PHOTO = 13;
    private static final int CAMERA_PIC_REQUEST = 16;
    private Uri fileUri;
    private String pathImage, mImageFileLocation = "";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_profile_edit, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {

        RequestOptions options = new RequestOptions()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.no_profile_image);

        if (!UserPreferences.loadUser(getActivity()).getUser_avatar().equalsIgnoreCase("")) {

            if (UserPreferences.loadUser(getActivity()).getUser_avatar().contains("https://lh3.googleusercontent.com/")) {
                Glide.with(getActivity())
                        .load(UserPreferences.loadUser(getActivity()).getUser_avatar())
                        .apply(options)
                        .into(imgVerifiedImage);

            } else {
                Glide.with(getActivity())
                        .load(APIInterface.API_DOMAIN + UserPreferences.loadUser(getActivity()).getUser_avatar())
                        .apply(options)
                        .into(imgVerifiedImage);
            }
        }

        txtVerifiedUserID.setText(UserPreferences.loadUser(getActivity()).getId());
        txtVerifiedName.setText(UserPreferences.loadUser(getActivity()).getFullName());
        txtVerifiedUsername.setText(UserPreferences.loadUser(getActivity()).getUsername() + "");
        txtVerifiedMobile.setText(UserPreferences.loadUser(getActivity()).getPhoneNumber());
        txtVerifiedEmail.setText(UserPreferences.loadUser(getActivity()).getEmail());

        imgUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new ChangeUsernameFragment());
            }
        });

        imgEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new ChangeEmailFragment());
            }
        });

        imgMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new ChangePhoneFragment());
            }
        });

        imgChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new ChangePasswordFragment());
            }
        });

        imgUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose your photo");
                String[] images = {"Take From Gallery", "Take From Camera"};
                builder.setItems(images, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO);
                                break;

                            case 1:
                                captureImage();
                                break;
                        }
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    private void captureImage() {
        if (Build.VERSION.SDK_INT > 21) { //use this if Lollipop_Mr1 (API 22) or above
            Intent callCameraApplicationIntent = new Intent();
            callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

            // We give some instruction to the intent to save the image
            File photoFile = null;

            // If the createImageFile will be successful, the photo file will have the address of the file
            photoFile = createImageFile();
            // Here we call the function that will try to catch the exception made by the throw function
            // Here we add an extra file to the intent to put the address on to. For this purpose we use the FileProvider, declared in the AndroidManifest.
            Uri outputUri = FileProvider.getUriForFile(
                    getActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    photoFile);
            callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

            // The following is a new line with a trying attempt
            callCameraApplicationIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

            Logger.getAnonymousLogger().info("Calling the camera App by intent");

            // The following strings calls the camera app and wait for his file in return.
            startActivityForResult(callCameraApplicationIntent, CAMERA_PIC_REQUEST);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

            // start the image capture Intent
            startActivityForResult(intent, CAMERA_PIC_REQUEST);
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Directory Error: ", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private File createImageFile() {
        Logger.getAnonymousLogger().info("Generating the image - method started");

        // Here we create a "non-collision file name", alternatively said, "an unique filename" using the "timeStamp" functionality
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmSS").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp;
        // Here we specify the environment location and the exact path where we want to save the so-created file
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/photo_saving_app");
        Logger.getAnonymousLogger().info("Storage directory set");

        // Then we create the storage directory if does not exists
        if (!storageDirectory.exists()) storageDirectory.mkdir();

        // Here we create the file using a prefix, a suffix and a directory
        File image = new File(storageDirectory, imageFileName + ".jpg");
        // File image = File.createTempFile(imageFileName, ".jpg", storageDirectory);

        // Here the location is saved into the string mImageFileLocation
        Logger.getAnonymousLogger().info("File name and path set");

        mImageFileLocation = image.getAbsolutePath();
        // fileUri = Uri.parse(mImageFileLocation);
        // The file is returned to the previous intent across the camera application
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.gc();

        if (requestCode == REQUEST_TAKE_PHOTO || requestCode == REQUEST_PICK_PHOTO) {
            if (data != null) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                pathImage = cursor.getString(columnIndex);
                imgVerifiedImage.setImageBitmap(BitmapFactory.decodeFile(pathImage));
                cursor.close();
            }

        } else if (requestCode == CAMERA_PIC_REQUEST) {
            if (Build.VERSION.SDK_INT > 21) {
                pathImage = mImageFileLocation;
                Glide.with(getActivity()).load(mImageFileLocation).into(imgVerifiedImage);

            } else {
                pathImage = fileUri.getPath();
                Glide.with(getActivity()).load(fileUri).into(imgVerifiedImage);
            }
        }

        changeProfileImage(pathImage);
    }

    private void changeProfileImage(String pathImage) {
        RequestBody userID = RequestBody.create(MediaType.parse("multipart/form-data"),
                UserPreferences.loadUser(getActivity()).getId());
        RequestBody phoneNumber = RequestBody.create(MediaType.parse("multipart/form-data"),
                UserPreferences.loadUser(getActivity()).getPhoneNumber());
        RequestBody email = RequestBody.create(MediaType.parse("multipart/form-data"),
                UserPreferences.loadUser(getActivity()).getEmail());
        RequestBody userName = RequestBody.create(MediaType.parse("multipart/form-data"),
                UserPreferences.loadUser(getActivity()).getUsername());
        RequestBody accountType = RequestBody.create(MediaType.parse("multipart/form-data"),
                "1");

        File file = new File(pathImage);
        RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part userAvatars =
                MultipartBody.Part.createFormData("user_avatars", file.getName(), mFile);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<String> apiCall = apiInterface.getUserUpdate(
                userID, phoneNumber, email, userName, userAvatars);

        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressbarManager.dismiss();

                if (response.isSuccessful()) {

                    try {
                        String res = response.body().toString();
                        Log.d("UpdateImage: ", res);

                        Gson gson = new Gson();
                        Reader reader = new StringReader(res);
                        ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                        if (profileModel.getProfileUserInfo().size() > 0) {
                            ProfileUserInfo profileUserInfo = profileModel.getProfileUserInfo().get(0);
                            UserPreferences.saveUser(getActivity(), profileUserInfo);
                        }

                        try {

                            final Dialog dialog = new Dialog(getActivity(), R.style.DialogTheme);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_comman);
                            dialog.setCancelable(false);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorBlack70)));
                            dialog.show();

                            TextView dialogMessage = dialog.findViewById(R.id.dialogMessage);
                            TextView dialogTitle = dialog.findViewById(R.id.dialogTitle);
                            ImageView dialogOK = dialog.findViewById(R.id.dialogOK);

                            dialogTitle.setText("Image updated!");
                            dialogMessage.setText("Image updated. You will need to login again to continue.");
                            dialogOK.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    activity.loadFragment(new HomeFragment());
                                }
                            });

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("ChangeEmail", t.getMessage().toString());
                Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());

        txtVerifiedUserID = (TextView) v.findViewById(R.id.txtVerifiedUserID);
        txtVerifiedName = (TextView) v.findViewById(R.id.txtVerifiedName);
        txtVerifiedUsername = (TextView) v.findViewById(R.id.txtVerifiedUsername);
        txtVerifiedMobile = (TextView) v.findViewById(R.id.txtVerifiedMobile);
        txtVerifiedEmail = (TextView) v.findViewById(R.id.txtVerifiedEmail);
        txtVerifiedChangePassword = (TextView) v.findViewById(R.id.txtVerifiedChangePassword);

        imgUsername = (ImageView) v.findViewById(R.id.imgUsername);
        imgMobile = (ImageView) v.findViewById(R.id.imgMobile);
        imgEmail = (ImageView) v.findViewById(R.id.imgEmail);
        imgChangePassword = (ImageView) v.findViewById(R.id.imgChangePassword);

        imgVerifiedImage = (CircularImageView) v.findViewById(R.id.imgVerifiedImage);
        imgUpload = (ImageView) v.findViewById(R.id.imgUpload);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

