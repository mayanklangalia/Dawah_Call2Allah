package com.calltoallah.dawah.fragment;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.TasbihReportAdapter;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.TasbihModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TasbihReportFragment extends BaseFragment {

    private RecyclerView mTasbihResult;
    private boolean responseFailed = false;
    private LinearLayout errorLayout;
    private Button btnAddTasbih;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_tasbih_report, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {
        getTasbih();

        btnAddTasbih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new TasbihCalcFragment());
            }
        });
    }

    private void getTasbih() {

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.getTasbih(UserPreferences.loadUser(getActivity()).getId());
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();
                        Log.d("Response: ", res);

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            TasbihModel tasbihModel = gson.fromJson(reader, TasbihModel.class);

                            if (tasbihModel.getTasbihEvents().size() > 0) {
                                mTasbihResult.setVisibility(View.VISIBLE);
                                errorLayout.setVisibility(View.GONE);

                                TasbihReportAdapter tasbihReportAdapter = new TasbihReportAdapter(activity, tasbihModel.getTasbihEvents());
                                mTasbihResult.setAdapter(tasbihReportAdapter);

                            } else {
                                mTasbihResult.setVisibility(View.GONE);
                                errorLayout.setVisibility(View.VISIBLE);
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            mTasbihResult.setVisibility(View.GONE);
                            errorLayout.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    mTasbihResult.setVisibility(View.GONE);
                    errorLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mTasbihResult.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void Init(View v) {
        errorLayout = v.findViewById(R.id.errorLayout);
        btnAddTasbih = v.findViewById(R.id.btnAddTasbih);

        mTasbihResult = (RecyclerView) v.findViewById(R.id.rcvTasbih);
        mTasbihResult.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mTasbihResult.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

