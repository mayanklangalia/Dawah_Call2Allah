package com.calltoallah.dawah.fragment;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.MasjidTransactionAdapter;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.model.MasjidTransactionListModel;
import com.calltoallah.dawah.model.MasjidTransactionModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;
import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletMasjidFragment extends BaseFragment {

    public static TextView txtWalletAmount, txtWithdrawalMessage;
    private RecyclerView mTransaction;
    private ProgressbarManager progressbarManager;
    private boolean responseFailed = false;
    private Bundle bundle;
    private MasjidListModel masjidListModel;
    private Button btnWidthdraw, btnDonateNow;
    private TextView txt0, txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txtResult;
    private ImageView imgBack, imgOK;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout contentLayout, errorLayout;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_wallet, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {

        bundle = getArguments();
        if (bundle != null) {

            btnWidthdraw.setVisibility(View.VISIBLE);
            masjidListModel = bundle.getParcelable(Constants.DATA);
            activity.mToolbarTitle.setText(masjidListModel.getMasjidName());
            getWidthdrawStatus();
        }

        progressbarManager.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getTransaction(masjidListModel.getId());
            }
        }, 1000);

        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getTransaction(masjidListModel.getId());
            }
        });

        btnDonateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new HomeFragment());
            }
        });

        btnWidthdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final StringBuilder sb = new StringBuilder();

                View parentView = getLayoutInflater().inflate(R.layout.dialog_withdraw, null);
                BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
                dialog.setContentView(parentView);
                dialog.show();

                txt0 = (TextView) dialog.findViewById(R.id.txt0);
                txt1 = (TextView) dialog.findViewById(R.id.txt1);
                txt2 = (TextView) dialog.findViewById(R.id.txt2);
                txt3 = (TextView) dialog.findViewById(R.id.txt3);
                txt4 = (TextView) dialog.findViewById(R.id.txt4);
                txt5 = (TextView) dialog.findViewById(R.id.txt5);
                txt6 = (TextView) dialog.findViewById(R.id.txt6);
                txt7 = (TextView) dialog.findViewById(R.id.txt7);
                txt8 = (TextView) dialog.findViewById(R.id.txt8);
                txt9 = (TextView) dialog.findViewById(R.id.txt9);
                txtResult = (TextView) dialog.findViewById(R.id.txtResult);

                imgBack = (ImageView) dialog.findViewById(R.id.imgBack);
                imgOK = (ImageView) dialog.findViewById(R.id.imgOK);

                txt0.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sb.append("0");
                        txtResult.setText(sb.toString());
                    }
                });

                txt1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sb.append("1");
                        txtResult.setText(sb.toString());
                    }
                });

                txt2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sb.append("2");
                        txtResult.setText(sb.toString());
                    }
                });

                txt3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sb.append("3");
                        txtResult.setText(sb.toString());
                    }
                });

                txt4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sb.append("4");
                        txtResult.setText(sb.toString());
                    }
                });

                txt5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sb.append("5");
                        txtResult.setText(sb.toString());
                    }
                });

                txt6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sb.append("6");
                        txtResult.setText(sb.toString());
                    }
                });

                txt7.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sb.append("7");
                        txtResult.setText(sb.toString());
                    }
                });

                txt8.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sb.append("8");
                        txtResult.setText(sb.toString());
                    }
                });

                txt9.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sb.append("9");
                        txtResult.setText(sb.toString());
                    }
                });

                imgBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (sb.length() != 0) {
                            txtResult.setText(sb.deleteCharAt(sb.length() - 1));
                        }
                    }
                });

                imgOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            int masjidAmount = Integer.parseInt(masjidListModel.getMasjidTotalAmount());
                            int resultAmount = Integer.parseInt(txtResult.getText().toString());

                            if (resultAmount == 0) {
                                Toast.makeText(getActivity(), "Enter Amount", Toast.LENGTH_SHORT).show();
                            } else if (txtResult.getText().toString().equalsIgnoreCase("")) {
                                Toast.makeText(getActivity(), "Enter Amount", Toast.LENGTH_SHORT).show();
                            } else if (masjidAmount < resultAmount) {
                                Toast.makeText(getActivity(), "Limit Exceeded", Toast.LENGTH_SHORT).show();
                            } else {
                                getAmountWithdrawalRequest(txtResult.getText().toString(), dialog);
                            }

                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Enter Amount", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    private void getAmountWithdrawalRequest(String amount, BottomSheetDialog dialog) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.addWithdrawalRequest(
                masjidListModel.getId(), amount,
                UserPreferences.loadUser(getActivity()).getId());
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    dialog.dismiss();

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                            Toast.makeText(getActivity(), "Withdrawal request has been submitted successfully!", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    private void getWidthdrawStatus() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getRequestMasjidWidthdrawStatus(masjidListModel.getId());
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                            int widthdrawSize = Integer.parseInt(jsonObject.getString("widrawal_request_size"));

                            if (widthdrawSize > 0) {
                                txtWithdrawalMessage.setVisibility(View.VISIBLE);
                                btnWidthdraw.setEnabled(false);
                            } else {
                                txtWithdrawalMessage.setVisibility(View.GONE);
                                btnWidthdraw.setEnabled(true);
                            }

                        } else {
                            Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    private void getTransaction(String masjidId) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.getMasjidTransactionHistory(masjidId);
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    swipeRefreshLayout.setRefreshing(false);
                    progressbarManager.dismiss();

                    try {
                        String res = response.body().toString();
                        Log.d("MASJID_WALLET: ", res);

                        try {
//                            Type listType = new TypeToken<ArrayList<LoginModel>>() {
//                            }.getType();
//                            ArrayList<LoginModel> workList = new Gson().fromJson(res, listType);

                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            MasjidTransactionModel masjidTransactionModel = gson.fromJson(reader, MasjidTransactionModel.class);

                            Collections.sort(masjidTransactionModel.getMasjidTransactionList(), new BY_DATE(true));
                            if (masjidTransactionModel.getMasjidTransactionList().size() > 0) {
                                contentLayout.setVisibility(View.VISIBLE);
                                errorLayout.setVisibility(View.GONE);

                                MasjidTransactionAdapter transactionAdapter = new MasjidTransactionAdapter(activity, masjidTransactionModel.getMasjidTransactionList());
                                mTransaction.setAdapter(transactionAdapter);

                                if (transactionAdapter.getWalletTotal(masjidTransactionModel.getMasjidTransactionList()) == 0) {
                                    txtWalletAmount.setText("₹ " + "0");
                                } else {
                                    txtWalletAmount.setText("₹ " + transactionAdapter.getWalletTotal(masjidTransactionModel.getMasjidTransactionList()) + "");
                                }

                            } else {
                                contentLayout.setVisibility(View.GONE);
                                errorLayout.setVisibility(View.VISIBLE);
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            contentLayout.setVisibility(View.GONE);
                            errorLayout.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    contentLayout.setVisibility(View.GONE);
                    errorLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progressbarManager.dismiss();
                contentLayout.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    public class BY_DATE implements Comparator<MasjidTransactionListModel> {
        private int mod = 1;

        public BY_DATE(boolean desc) {
            if (desc) mod = -1;
        }

        @Override
        public int compare(MasjidTransactionListModel arg0, MasjidTransactionListModel arg1) {
            return mod * arg0.getDateTime().compareTo(arg1.getDateTime());
        }
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());
        swipeRefreshLayout = v.findViewById(R.id.swipeRefresh);
        txtWalletAmount = (TextView) v.findViewById(R.id.txtWalletAmount);
        txtWithdrawalMessage = (TextView) v.findViewById(R.id.txtWithdrawalMessage);
        btnWidthdraw = (Button) v.findViewById(R.id.btnWidthdraw);
        btnDonateNow = (Button) v.findViewById(R.id.btnDonateNow);
        contentLayout = v.findViewById(R.id.contentLayout);
        errorLayout = v.findViewById(R.id.errorLayout);

        mTransaction = (RecyclerView) v.findViewById(R.id.rcvTransaction);
        mTransaction.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mTransaction.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

