package com.calltoallah.dawah.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.icu.util.IslamicCalendar;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.calendar.EventAdapter;
import com.calltoallah.dawah.calendar.EventDecorator;
import com.calltoallah.dawah.calendar.HighlightWeekendsDecorator;
import com.calltoallah.dawah.calendar.MySelectorDecorator;
import com.calltoallah.dawah.calendar.OneDayDecorator;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.CalendarEventListModel;
import com.calltoallah.dawah.model.CalendarModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.github.eltohamy.materialhijricalendarview.CalendarDay;
import com.github.eltohamy.materialhijricalendarview.MaterialHijriCalendarView;
import com.github.eltohamy.materialhijricalendarview.OnDateSelectedListener;
import com.github.eltohamy.materialhijricalendarview.OnMonthChangedListener;
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalendarFragment extends BaseFragment {
    private MaterialHijriCalendarView mCalendarView;
    private RecyclerView mRecyclerView;

    private final OneDayDecorator oneDayDecorator = new OneDayDecorator();
    private List<CalendarEventListModel> mCalendarModel = new ArrayList<>();
    private List<CalendarEventListModel> mFilterList = new ArrayList<>();
    private EventAdapter eventAdapter;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_calender, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setData() {

        mCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialHijriCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                //If you change a decorate, you need to invalidate decorators
                oneDayDecorator.setDate(date.getDate());
                widget.invalidateDecorators();

                if (UserPreferences.loadUser(getActivity()).getId().equalsIgnoreCase("1")) {
                    String selectedDate = getMonth(widget.getSelectedDate().getMonth()) + " - "
                            + widget.getSelectedDate().getDay() + ", " + widget.getSelectedDate().getYear();
                    addEvent(selectedDate);
                } else {
                    // Toast.makeText(activity, "Only admin can add this events!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mCalendarView.setShowOtherDates(MaterialHijriCalendarView.SHOW_ALL);

        Calendar calendar = Calendar.getInstance();
        mCalendarView.setSelectedDate(calendar.getTime());

        calendar.set(calendar.get(Calendar.YEAR), Calendar.JANUARY, 1);
        mCalendarView.setMinimumDate(calendar.getTime());

        calendar.set(calendar.get(Calendar.YEAR), Calendar.DECEMBER, 31);
        mCalendarView.setMaximumDate(calendar.getTime());

        mCalendarView.addDecorators(
                new MySelectorDecorator(getActivity()),
                new HighlightWeekendsDecorator(),
                oneDayDecorator
        );

        loadCalendarEvents();

        mCalendarView.setOnMonthChangedListener(new OnMonthChangedListener() {

            @Override
            public void onMonthChanged(MaterialHijriCalendarView widget, CalendarDay date) {
                mFilterList = new ArrayList<>();
                for (CalendarEventListModel calendarModel : mCalendarModel) {
                    if (calendarModel.getEventMonth() == date.getMonth()) {
                        mFilterList.add(calendarModel);
                        if (mFilterList.size() > 0) {
                            eventAdapter = new EventAdapter(getActivity(), mFilterList);
                            mRecyclerView.setAdapter(eventAdapter);
                            eventAdapter.notifyDataSetChanged();
                        }

                    } else {
                        mFilterList.remove(calendarModel);
                        eventAdapter = new EventAdapter(getActivity(), mFilterList);
                        mRecyclerView.setAdapter(eventAdapter);
                        eventAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    private void addEvent(String selectedDate) {
        View parentView = getLayoutInflater().inflate(R.layout.dialog_event, null);
        BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(parentView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog dialogc = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = dialogc.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        dialog.show();

        EditText edtEventName = (EditText) dialog.findViewById(R.id.edtEventName);
        Button btnAddEvent = (Button) dialog.findViewById(R.id.btnAddEvent);

        EditText edtEventDate = (EditText) dialog.findViewById(R.id.edtEventDate);
        edtEventDate.setText(selectedDate);

        ImageView pickDate = (ImageView) dialog.findViewById(R.id.pickDate);
        pickDate.setVisibility(View.GONE);

        btnAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                final Call<String> apiCall = apiInterface.addCalender(
                        edtEventDate.getText().toString(), edtEventName.getText().toString(),
                        UserPreferences.loadUser(getActivity()).getId());
                apiCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            dialog.dismiss();
                            startActivity(new Intent(getActivity(), MainActivity.class));
                            Toast.makeText(getActivity(), "Event added successfully!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                    }
                });
            }
        });

    }

    private void loadCalendarEvents() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.getCalender();
        apiCall.enqueue(new Callback<String>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    String res = response.body().toString();
                    Log.d("Calendar: ", res + "");

                    try {
                        Gson gson = new Gson();
                        Reader reader = new StringReader(res);
                        CalendarModel calendarModel = gson.fromJson(reader, CalendarModel.class);

                        List<CalendarEventListModel> calendarEventListModel = calendarModel.getCalendarEventListModel();
                        if (calendarEventListModel.size() > 0) {
                            mCalendarModel = calendarModel.getCalendarEventListModel();

                            Calendar gregorianCal = Calendar.getInstance();
                            IslamicCalendar cal = new IslamicCalendar();
                            cal.setTime(gregorianCal.getTime());

                            int month = cal.get(Calendar.MONTH);
                            List<CalendarEventListModel> mTemp = new ArrayList<>();
                            for (CalendarEventListModel eventListModel : calendarEventListModel) {

                                if (eventListModel.getEventDate().equalsIgnoreCase("0000-00-00")) {
                                    // Do nothing ....
                                } else {
                                    int eventMonth = getMonth(eventListModel.getEventDate().split("-")[0].trim());
                                    int eventDay = Integer.valueOf(eventListModel.getEventDate().split("-")[1].split(",")[0].trim());
                                    int eventYear = Integer.valueOf(eventListModel.getEventDate().split(",")[1].trim());

                                    eventListModel.setEventDay(eventDay);
                                    eventListModel.setEventMonth(eventMonth);
                                    eventListModel.setEventYear(eventYear);

                                    if (eventMonth == month) {
                                        mTemp.add(eventListModel);
                                        if (mTemp.size() > 0) {
                                            eventAdapter = new EventAdapter(getActivity(), mTemp);
                                            mRecyclerView.setAdapter(eventAdapter);
                                            eventAdapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }

                            new ApiSimulator().executeOnExecutor(Executors.newSingleThreadExecutor());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    private int getMonth(String eventMonth) {
        int islamicMonth = 0;
        if (eventMonth.contains(getResources().getStringArray(R.array.custom_months)[0])) {
            islamicMonth = UmmalquraCalendar.MUHARRAM;
        } else if (eventMonth.contains(getResources().getStringArray(R.array.custom_months)[1])) {
            islamicMonth = UmmalquraCalendar.SAFAR;
        } else if (eventMonth.contains(getResources().getStringArray(R.array.custom_months)[2])) {
            islamicMonth = UmmalquraCalendar.RABI_AWWAL;
        } else if (eventMonth.contains(getResources().getStringArray(R.array.custom_months)[3])) {
            islamicMonth = UmmalquraCalendar.RABI_THANI;
        } else if (eventMonth.contains(getResources().getStringArray(R.array.custom_months)[4])) {
            islamicMonth = UmmalquraCalendar.JUMADA_AWWAL;
        } else if (eventMonth.contains(getResources().getStringArray(R.array.custom_months)[5])) {
            islamicMonth = UmmalquraCalendar.JUMADA_THANI;
        } else if (eventMonth.contains(getResources().getStringArray(R.array.custom_months)[6])) {
            islamicMonth = UmmalquraCalendar.RAJAB;
        } else if (eventMonth.contains(getResources().getStringArray(R.array.custom_months)[7])) {
            islamicMonth = UmmalquraCalendar.SHAABAN;
        } else if (eventMonth.contains(getResources().getStringArray(R.array.custom_months)[8])) {
            islamicMonth = UmmalquraCalendar.RAMADHAN;
        } else if (eventMonth.contains(getResources().getStringArray(R.array.custom_months)[9])) {
            islamicMonth = UmmalquraCalendar.SHAWWAL;
        } else if (eventMonth.contains(getResources().getStringArray(R.array.custom_months)[10])) {
            islamicMonth = UmmalquraCalendar.THUL_QIDAH;
        } else if (eventMonth.contains(getResources().getStringArray(R.array.custom_months)[11])) {
            islamicMonth = UmmalquraCalendar.THUL_HIJJAH;
        }
        return islamicMonth;
    }

    private String getMonth(int event_month) {
        String islamicMonth = "";
        if (event_month == 0) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[0];
        } else if (event_month == 1) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[1];
        } else if (event_month == 2) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[2];
        } else if (event_month == 3) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[3];
        } else if (event_month == 4) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[4];
        } else if (event_month == 5) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[5];
        } else if (event_month == 6) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[6];
        } else if (event_month == 7) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[7];
        } else if (event_month == 8) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[8];
        } else if (event_month == 9) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[9];
        } else if (event_month == 10) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[10];
        } else if (event_month == 11) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[11];
        }
        return islamicMonth;
    }

    private void Init(View v) {
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mCalendarView = (MaterialHijriCalendarView) v.findViewById(R.id.calendarView);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }

    private class ApiSimulator extends AsyncTask<Void, Void, List<CalendarDay>> {

        @Override
        protected List<CalendarDay> doInBackground(@NonNull Void... voids) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ArrayList<CalendarDay> dates = new ArrayList<>();
            for (int i = 0; i < mCalendarModel.size(); i++) {
                CalendarEventListModel calendarModel = mCalendarModel.get(i);

                if (calendarModel.getEventDate().equalsIgnoreCase("0000-00-00")) {
                    // Do nothing ....
                } else {
                    UmmalquraCalendar ummalquraCalendar = new UmmalquraCalendar();
                    ummalquraCalendar.set(Calendar.YEAR, calendarModel.getEventYear());
                    ummalquraCalendar.set(Calendar.MONTH, calendarModel.getEventMonth());
                    ummalquraCalendar.set(Calendar.DAY_OF_MONTH, calendarModel.getEventDay());
                    CalendarDay day = CalendarDay.from(ummalquraCalendar);
                    dates.add(day);
                }
            }

            return dates;
        }

        @Override
        protected void onPostExecute(@NonNull List<CalendarDay> calendarDays) {
            super.onPostExecute(calendarDays);
            mCalendarView.addDecorator(new EventDecorator(Color.RED, calendarDays));
        }
    }
}
