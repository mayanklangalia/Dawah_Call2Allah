package com.calltoallah.dawah.fragment;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.TopFundAdapter;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.FundModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;

import java.io.Reader;
import java.io.StringReader;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopFundRaiserFragment extends BaseFragment {

    private RecyclerView mFundList;
    private LinearLayout errorLayout;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_fund, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        Init(v);
        setData();
    }

    private void setData() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.getFundRaiser(UserPreferences.loadUser(getActivity()).getId());
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    Log.d("TopFundRaiser: ", response.body().toString());

                    try {
                        Gson gson = new Gson();
                        Reader reader = new StringReader(response.body().toString());
                        FundModel fundModel = gson.fromJson(reader, FundModel.class);

                        if (fundModel.getFundraiserList().size() > 0) {
                            mFundList.setVisibility(View.VISIBLE);
                            errorLayout.setVisibility(View.GONE);

                            TopFundAdapter topFundAdapter = new TopFundAdapter(activity, fundModel.getFundraiserList());
                            mFundList.setAdapter(topFundAdapter);

                        } else{
                            mFundList.setVisibility(View.GONE);
                            errorLayout.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                mFundList.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        });

    }

    private void Init(View v) {
        errorLayout = (LinearLayout) v.findViewById(R.id.errorLayout);
        mFundList = (RecyclerView) v.findViewById(R.id.rcvFund);
        mFundList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mFundList.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

