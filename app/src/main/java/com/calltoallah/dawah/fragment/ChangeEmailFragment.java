package com.calltoallah.dawah.fragment;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.ProfileModel;
import com.calltoallah.dawah.model.ProfileUserInfo;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangeEmailFragment extends BaseFragment {

    private String TAG = ChangeEmailFragment.class.getSimpleName();
    private TextView txtOldEmail;
    private EditText editNewEmail;
    private Button btnChangeEmail;
    private ProgressbarManager progressbarManager;
    private boolean responseFailed = false;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_change_email, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        Init(v);
        setData();
    }

    private void setData() {
        if (UserPreferences.loadUser(getActivity()).getEmail() != null) {
            txtOldEmail.setText(UserPreferences.loadUser(getActivity()).getEmail());
        }

        btnChangeEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editNewEmail.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(activity, "Email should not be blank!", Toast.LENGTH_SHORT).show();
                } else {
                    progressbarManager.show();
                    changeEmail();
                }
            }
        });
    }

    private void changeEmail() {
        RequestBody userID = RequestBody.create(MediaType.parse("multipart/form-data"),
                UserPreferences.loadUser(getActivity()).getId());
        RequestBody phoneNumber = RequestBody.create(MediaType.parse("multipart/form-data"),
                UserPreferences.loadUser(getActivity()).getPhoneNumber());
        RequestBody email = RequestBody.create(MediaType.parse("multipart/form-data"),
                editNewEmail.getText().toString());
        RequestBody userName = RequestBody.create(MediaType.parse("multipart/form-data"),
                UserPreferences.loadUser(getActivity()).getUsername());

        File file = new File("");
        RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part userAvatars =
                MultipartBody.Part.createFormData("user_avatars", file.getName(), mFile);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<String> apiCall = apiInterface.getUserUpdate(
                userID, phoneNumber, email, userName, null);

        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressbarManager.dismiss();

                if (response.isSuccessful()) {

                    try {
                        String res = response.body().toString();
                        Log.d("UpdateEmail: ", res);

                        Gson gson = new Gson();
                        Reader reader = new StringReader(res);
                        ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                        if (profileModel.getProfileUserInfo().size() > 0) {
                            ProfileUserInfo profileUserInfo = profileModel.getProfileUserInfo().get(0);
                            UserPreferences.saveUser(getActivity(), profileUserInfo);
                        }

                        try {

                            final Dialog dialog = new Dialog(getActivity(), R.style.DialogTheme);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_comman);
                            dialog.setCancelable(false);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorBlack70)));
                            dialog.show();

                            TextView dialogMessage = dialog.findViewById(R.id.dialogMessage);
                            TextView dialogTitle = dialog.findViewById(R.id.dialogTitle);
                            ImageView dialogOK = dialog.findViewById(R.id.dialogOK);

                            dialogTitle.setText("Email updated!");
                            dialogMessage.setText("Email address updated. Please check your new email and login again to continue.");
                            dialogOK.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    activity.loadFragment(new HomeFragment());
                                }
                            });

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("ChangeEmail",  t.getMessage().toString());
                Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());
        txtOldEmail = (TextView) v.findViewById(R.id.txtOldEmail);
        editNewEmail = (EditText) v.findViewById(R.id.editNewEmail);
        btnChangeEmail = (Button) v.findViewById(R.id.btnChangeEmail);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}
