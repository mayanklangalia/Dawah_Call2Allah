package com.calltoallah.dawah.fragment;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.calltoallah.dawah.BuildConfig;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.comman.UserSignature;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.calltoallah.dawah.stepview.StepView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Logger;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class MasjidVerificationFragment extends BaseFragment {

    private StepView mStepView;
    private int currentStep = 0;
    private Button btnPrev, btnNext;
    private LinearLayout linear1, linear2, linear3, linear4, linearSignature;
    private UserSignature signatureView;

    private EditText edtMasjidName, edtRegistrationNumber;
    private EditText edtBankName, edtAccountHolderName, edtAccountNumber, edtIFSCCode;
    private ImageView imagePancard, imageVideo;
    private Button takePancard, takeVideo;

    private static final String IMAGE_DIRECTORY_NAME = "Dawah";
    private static final int PANCARD_PIC_REQUEST = 01;
    private static final int REQUEST_PICK_VIDEO = 02;

    private Uri fileUri;
    private String pathPAN, mImageFileLocation = "", videoPath, signaturePath;
    private ProgressbarManager progressbarManager;

    private MasjidListModel masjidListModel;
    private Bundle bundle;
    private int mode;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_masjid_verification, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);

        bundle = getArguments();
        if (bundle != null) {
            masjidListModel = bundle.getParcelable(Constants.DATA);
            mode = bundle.getInt(Constants.MODE);
        }

        configData();
    }

    private void configData() {
        edtMasjidName.setText(masjidListModel.getMasjidName());

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentStep < mStepView.getStepCount() - 1) {

                    if (currentStep == 0) {
                        if (edtMasjidName.getText().toString().equalsIgnoreCase("")) {
                            Toast.makeText(activity, "Please enter masjid/madrasah name", Toast.LENGTH_SHORT).show();

                        } else if (edtRegistrationNumber.getText().toString().equalsIgnoreCase("")) {
                            Toast.makeText(activity, "Please enter registration number", Toast.LENGTH_SHORT).show();

                        } else if (edtBankName.getText().toString().equalsIgnoreCase("")) {
                            Toast.makeText(activity, "Please enter bank name", Toast.LENGTH_SHORT).show();

                        } else if (edtAccountHolderName.getText().toString().equalsIgnoreCase("")) {
                            Toast.makeText(activity, "Please enter account holder name", Toast.LENGTH_SHORT).show();

                        } else if (edtAccountNumber.getText().toString().equalsIgnoreCase("")) {
                            Toast.makeText(activity, "Please enter account number", Toast.LENGTH_SHORT).show();

                        } else if (edtIFSCCode.getText().toString().equalsIgnoreCase("")) {
                            Toast.makeText(activity, "Please enter ifsc code", Toast.LENGTH_SHORT).show();

                        } else {
                            currentStep++;
                            mStepView.go(currentStep, true);
                            configPage(currentStep);
                        }

                    } else if (currentStep == 1) {

                        if (pathPAN == null) {
                            Toast.makeText(activity, "Please choose pan image", Toast.LENGTH_SHORT).show();

                        } else {
                            currentStep++;
                            mStepView.go(currentStep, true);
                            configPage(currentStep);
                        }

                    } else if (currentStep == 2) {

                        if (videoPath == null) {
                            Toast.makeText(activity, "Please choose short video", Toast.LENGTH_SHORT).show();

                        } else {
                            currentStep++;
                            mStepView.go(currentStep, true);
                            configPage(currentStep);
                        }

                    } else {

                        if (signaturePath == null) {
                            Toast.makeText(activity, "Please signature required", Toast.LENGTH_SHORT).show();
                        } else {
                            currentStep++;
                            mStepView.go(currentStep, true);
                            configPage(currentStep);
                        }
                    }

                } else {
                    mStepView.done(true);
                    btnNext.setText("DONE");

                    progressbarManager.show();
                    requestVerification();
                }
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentStep > 0) {
                    currentStep--;
                }
                mStepView.done(false);
                mStepView.go(currentStep, true);
                btnNext.setText("NEXT");

                configPage(currentStep);
            }
        });
    }

    private void requestVerification() {
        if (signatureView.getSignature() != null) {
            Uri tempUri = getSignatureUri(getActivity(), signatureView.getSignature());
            signaturePath = getSignRealPathFromURI(tempUri);
        }

        RequestBody masjidID = RequestBody.create(MediaType.parse("multipart/form-data"),
                masjidListModel.getId());
        RequestBody masjidName = RequestBody.create(MediaType.parse("multipart/form-data"),
                masjidListModel.getMasjidName());
        RequestBody masjidRegNumber = RequestBody.create(MediaType.parse("multipart/form-data"),
                edtRegistrationNumber.getText().toString().trim());
        RequestBody userIdentifier = RequestBody.create(MediaType.parse("multipart/form-data"),
                UserPreferences.loadUser(getActivity()).getId());
        RequestBody bankName = RequestBody.create(MediaType.parse("multipart/form-data"),
                edtBankName.getText().toString());
        RequestBody accountHolderName = RequestBody.create(MediaType.parse("multipart/form-data"),
                edtAccountHolderName.getText().toString());
        RequestBody accountNumber = RequestBody.create(MediaType.parse("multipart/form-data"),
                edtAccountNumber.getText().toString());
        RequestBody ifsccode = RequestBody.create(MediaType.parse("multipart/form-data"),
                edtIFSCCode.getText().toString());

        // pan Info ...
        File file2 = new File(pathPAN);
        RequestBody mFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2);
        MultipartBody.Part panFront =
                MultipartBody.Part.createFormData("pan_card_photo", file2.getName(), mFile2);

        // video Info ...
        File file3 = new File(videoPath);
        RequestBody mFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file3);
        MultipartBody.Part videoFile =
                MultipartBody.Part.createFormData("short_video", file3.getName(), mFile3);

        // signature Info ...
        File file4 = new File(signaturePath);
        RequestBody mFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), file4);
        MultipartBody.Part signatureFile =
                MultipartBody.Part.createFormData("signature_photo", file4.getName(), mFile4);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.addMasjidVerification(
                masjidID, masjidName, masjidRegNumber,
                videoFile, panFront, signatureFile, userIdentifier,
                bankName, accountHolderName,
                accountNumber, ifsccode);
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("MasjidVerification: ", response.body().toString());
                progressbarManager.dismiss();

                if (response.isSuccessful()) {
                    Toast.makeText(getActivity(), "Masjid verification successfully registered.", Toast.LENGTH_SHORT).show();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.DATA, masjidListModel);
                    bundle.putInt(Constants.MODE, mode);

                    MasjidDetailsFragment masjidDetailsFragment = new MasjidDetailsFragment();
                    masjidDetailsFragment.setArguments(bundle);
                    activity.loadFragment(masjidDetailsFragment);
                } else {
                    Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                progressbarManager.dismiss();
            }
        });

    }

    private void configPage(int currentStep) {
        if (currentStep == 0) {
            linear1.setVisibility(View.VISIBLE);
            linear2.setVisibility(View.GONE);
            linear3.setVisibility(View.GONE);
            linear4.setVisibility(View.GONE);
        } else if (currentStep == 1) {
            linear2.setVisibility(View.VISIBLE);
            linear1.setVisibility(View.GONE);
            linear3.setVisibility(View.GONE);
            linear4.setVisibility(View.GONE);
        } else if (currentStep == 2) {
            linear3.setVisibility(View.VISIBLE);
            linear1.setVisibility(View.GONE);
            linear2.setVisibility(View.GONE);
            linear4.setVisibility(View.GONE);
        } else {
            linear4.setVisibility(View.VISIBLE);
            linear1.setVisibility(View.GONE);
            linear2.setVisibility(View.GONE);
            linear3.setVisibility(View.GONE);
        }

        takePancard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, PANCARD_PIC_REQUEST);
            }
        });

        takeVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                pickVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
                startActivityForResult(pickVideoIntent, REQUEST_PICK_VIDEO);
            }
        });
    }

    public String getSignRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public Uri getSignatureUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, UserPreferences.loadUser(getActivity()).getId() + "_Sign", null);
        return Uri.parse(path);
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());
        signatureView = new UserSignature(getActivity());

        mStepView = v.findViewById(R.id.stepView);
        btnPrev = v.findViewById(R.id.btnPrev);
        btnNext = v.findViewById(R.id.btnNext);

        linear1 = (LinearLayout) v.findViewById(R.id.linear1);
        linear2 = (LinearLayout) v.findViewById(R.id.linear2);
        linear3 = (LinearLayout) v.findViewById(R.id.linear3);
        linear4 = (LinearLayout) v.findViewById(R.id.linear4);

        linearSignature = (LinearLayout) v.findViewById(R.id.linearSignature);
        linearSignature.addView(signatureView);

        edtMasjidName = (EditText) v.findViewById(R.id.edtMasjidName);
        edtRegistrationNumber = (EditText) v.findViewById(R.id.edtRegistrationNumber);

        edtBankName = (EditText) v.findViewById(R.id.edtBankName);
        edtAccountHolderName = (EditText) v.findViewById(R.id.edtAccountHolderName);
        edtAccountNumber = (EditText) v.findViewById(R.id.edtAccountNumber);
        edtIFSCCode = (EditText) v.findViewById(R.id.edtIFSCCode);

        imagePancard = (ImageView) v.findViewById(R.id.imagePancard);
        imageVideo = (ImageView) v.findViewById(R.id.imageVideo);

        takePancard = (Button) v.findViewById(R.id.takePancard);
        takeVideo = (Button) v.findViewById(R.id.takeVideo);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);

//        Bundle bundle = new Bundle();
//        bundle.putParcelable(Constants.DATA, masjidListModel);
//
//        MasjidDetailsFragment masjidDetailsFragment = new MasjidDetailsFragment();
//        masjidDetailsFragment.setArguments(bundle);
//        activity.loadFragment(masjidDetailsFragment);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.gc();

        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_PICK_VIDEO) {
                if (data != null) {
                    Uri selectedVideo = data.getData();

                    videoPath = getSignRealPathFromURI(selectedVideo);
                    imageVideo.setImageBitmap(ThumbnailUtils.createVideoThumbnail(
                            videoPath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND));
                }
            }

            // PAN CARD
            if (requestCode == PANCARD_PIC_REQUEST) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                pathPAN = cursor.getString(columnIndex);

                Glide.with(getActivity()).load(pathPAN).into(imagePancard);
                cursor.close();
            }
        }
    }


    private void captureImage(int requestCode) {
        if (Build.VERSION.SDK_INT > 21) { //use this if Lollipop_Mr1 (API 22) or above
            Intent callCameraApplicationIntent = new Intent();
            callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

            // We give some instruction to the intent to save the image
            File photoFile = null;

            // If the createImageFile will be successful, the photo file will have the address of the file
            photoFile = createImageFile();
            // Here we call the function that will try to catch the exception made by the throw function
            // Here we add an extra file to the intent to put the address on to. For this purpose we use the FileProvider, declared in the AndroidManifest.
            Uri outputUri = FileProvider.getUriForFile(
                    getActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    photoFile);
            callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

            // The following is a new line with a trying attempt
            callCameraApplicationIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

            Logger.getAnonymousLogger().info("Calling the camera App by intent");

            // The following strings calls the camera app and wait for his file in return.
            startActivityForResult(callCameraApplicationIntent, requestCode);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

            // start the image capture Intent
            startActivityForResult(intent, requestCode);
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Directory Error: ", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private File createImageFile() {
        Logger.getAnonymousLogger().info("Generating the image - method started");

        // Here we create a "non-collision file name", alternatively said, "an unique filename" using the "timeStamp" functionality
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmSS").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp;
        // Here we specify the environment location and the exact path where we want to save the so-created file
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/photo_saving_app");
        Logger.getAnonymousLogger().info("Storage directory set");

        // Then we create the storage directory if does not exists
        if (!storageDirectory.exists()) storageDirectory.mkdir();

        // Here we create the file using a prefix, a suffix and a directory
        File image = new File(storageDirectory, imageFileName + ".jpg");
        // File image = File.createTempFile(imageFileName, ".jpg", storageDirectory);

        // Here the location is saved into the string mImageFileLocation
        Logger.getAnonymousLogger().info("File name and path set");

        mImageFileLocation = image.getAbsolutePath();
        // fileUri = Uri.parse(mImageFileLocation);
        // The file is returned to the previous intent across the camera application
        return image;
    }
}
