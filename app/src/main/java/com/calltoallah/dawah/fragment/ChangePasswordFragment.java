package com.calltoallah.dawah.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.calltoallah.dawah.LoginActivity;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.comman.UserPreferences.clearUser;

public class ChangePasswordFragment extends BaseFragment {

    private EditText edtNewEmail, edtConfEmail;
    private Button btnChangePassword;
    private ProgressbarManager progressbarManager;
    private boolean responseFailed = false;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_change_password, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtNewEmail.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter new password!", Toast.LENGTH_SHORT).show();

                } else if (edtConfEmail.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter confirm password!", Toast.LENGTH_SHORT).show();

                } else if (!edtNewEmail.getText().toString().equalsIgnoreCase(edtConfEmail.getText().toString())) {
                    Toast.makeText(getActivity(), "New and confirm password should be same!", Toast.LENGTH_SHORT).show();

                } else {
                    progressbarManager.show();
                    UserChangeRequest();
                }
            }
        });
    }

    private void UserChangeRequest() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.getUserPasswordChange(
                UserPreferences.loadUser(getActivity()).getId(),
                edtNewEmail.getText().toString());

        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    progressbarManager.dismiss();

                    try {
                        String res = response.body().toString();
                        Log.d("ChangePassword: ", res);

                        try {

                            JSONObject jsonObject = new JSONObject(res);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                Toast.makeText(getActivity(), "Password updated successfully!", Toast.LENGTH_SHORT).show();

                                activity.saveLogin(Constants.LOGIN, 0);
                                clearUser(getActivity());

                                Intent intent = new Intent(getActivity(),
                                        LoginActivity.class);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                activity.finish();

                            } else {
                                Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            progressbarManager.dismiss();
                            Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    progressbarManager.dismiss();
                    Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progressbarManager.dismiss();
                Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());

        edtNewEmail = (EditText) v.findViewById(R.id.edtNewPassword);
        edtConfEmail = (EditText) v.findViewById(R.id.edtConfirmPassword);
        btnChangePassword = (Button) v.findViewById(R.id.btnConfirm);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

