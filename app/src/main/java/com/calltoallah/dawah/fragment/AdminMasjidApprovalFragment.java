package com.calltoallah.dawah.fragment;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.AdminMasjidApprovalAdapter;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.MasjidModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminMasjidApprovalFragment extends BaseFragment {

    private boolean responseFailed = false;
    private ProgressbarManager progressbarManager;
    private RecyclerView mMasjidApproval;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_admin_masjid_approval, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        Init(v);

        progressbarManager.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getAdminApproval();
            }
        }, 1000);
    }

    private void getAdminApproval() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.getApprovalList(UserPreferences.loadUser(getActivity()).getId());
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressbarManager.dismiss();

                if (response.isSuccessful()) {

                    try {
                        String res = response.body().toString();
                        Log.d("APPROVAL: ", res);

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            final MasjidModel masjidModel = gson.fromJson(reader, MasjidModel.class);

                            if (masjidModel.getMasjidListModel().size() > 0) {

                                AdminMasjidApprovalAdapter adminMasjidApprovalAdapter = new AdminMasjidApprovalAdapter(activity, masjidModel.getMasjidListModel());
                                mMasjidApproval.setAdapter(adminMasjidApprovalAdapter);
                                adminMasjidApprovalAdapter.notifyDataSetChanged();

//                                adminMasjidApprovalAdapter.setOnItemClickListener(new MasjidAdapter.OnItemClickListener() {
//                                    @Override
//                                    public void onItemClick(View view, int position) {
//                                        Bundle bundle = new Bundle();
//                                        bundle.putParcelable(Constants.DATA, masjidModel.getMasjidListModel().get(position));
//
//                                        MasjidDetailsFragment masjidDetailsFragment = new MasjidDetailsFragment();
//                                        masjidDetailsFragment.setArguments(bundle);
//                                        activity.loadFragment(masjidDetailsFragment);
//                                    }
//                                });

                            } else {
                                Toast.makeText(activity, "Data not found!", Toast.LENGTH_SHORT).show();
                            }


                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progressbarManager.dismiss();
                Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());
        mMasjidApproval = (RecyclerView) v.findViewById(R.id.rcvMasjidApproval);
        mMasjidApproval.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mMasjidApproval.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

