package com.calltoallah.dawah.comman;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.calltoallah.dawah.R;

public class ProgressbarManager extends Dialog {

	private ProgressBar progressBar;
	private TextView tv;

	public ProgressbarManager(Context context) {
		super(context, R.style.TransparentProgressDialog);
		WindowManager.LayoutParams wlmp = getWindow().getAttributes();
		wlmp.gravity = Gravity.CENTER_HORIZONTAL;
		getWindow().setAttributes(wlmp);
//		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

		setTitle(null);
		setCancelable(false);
		setOnCancelListener(null);
		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		progressBar = new ProgressBar(context);
		progressBar.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.jz_loading));

		tv = new TextView(context);
		tv.setText("Loading...");
		tv.setTextAppearance(context, android.R.style.TextAppearance_Medium);
		tv.setTextColor(context.getResources().getColor(R.color.colorOffWhite));
		tv.setGravity(Gravity.CENTER);

		layout.addView(progressBar, params);
		layout.addView(tv, params);

		addContentView(layout, params);
	}
}