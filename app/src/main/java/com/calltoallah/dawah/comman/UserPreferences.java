package com.calltoallah.dawah.comman;

import android.content.Context;
import android.content.SharedPreferences;

import com.calltoallah.dawah.model.ProfileUserInfo;

import java.io.Serializable;

public class UserPreferences implements Serializable {
    public static final String PREFS_NAME = Constants.USERINFO;
    public static final String DEFAULT_VAL = null;

    public static void saveUser(Context context, ProfileUserInfo user) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor dbEditor = prefs.edit();

        dbEditor.putString("id", user.getId());
        dbEditor.putString("full_name", user.getFullName());
        dbEditor.putString("phone_number", user.getPhoneNumber());
        dbEditor.putString("email", user.getEmail());
        dbEditor.putString("address", user.getAddress());
        dbEditor.putString("username", user.getUsername());
        dbEditor.putString("password", user.getPassword());
        dbEditor.putString("status", user.getStatus());
        dbEditor.putString("verified_status", user.getVerify_status());
        dbEditor.putString("pan_card_number", user.getPan_card_number());
        dbEditor.putString("uidai_number", user.getUidai_number());
        dbEditor.putString("front_photo_id", user.getFront_photo_id());
        dbEditor.putString("back_photo_id", user.getBack_photo_id());
        dbEditor.putString("short_video", user.getShort_video());
        dbEditor.putString("pan_card_photo", user.getPan_card_photo());
        dbEditor.putString("signature_photo", user.getSignature_photo());
        dbEditor.putString("user_avatar", user.getUser_avatar());
        dbEditor.putString("created_on", user.getCreatedOn());
        dbEditor.putString("user_token", user.getUserToken());
        dbEditor.putString("share_code", user.getShareCode());
        dbEditor.putString("wallet_total", user.getWalletTotal());
        dbEditor.putString("user_donated", user.getUser_donated());
        dbEditor.putString("follower_count", user.getFollower_count());
        dbEditor.putString("following_count", user.getFollowing_count());

        dbEditor.apply();
        dbEditor.commit();
    }

    public static ProfileUserInfo loadUser(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        ProfileUserInfo user = new ProfileUserInfo();
        user.setId(prefs.getString("id", DEFAULT_VAL));
        user.setFullName(prefs.getString("full_name", DEFAULT_VAL));
        user.setPhoneNumber(prefs.getString("phone_number", DEFAULT_VAL));
        user.setEmail(prefs.getString("email", DEFAULT_VAL));
        user.setAddress(prefs.getString("address", DEFAULT_VAL));
        user.setUsername(prefs.getString("username", DEFAULT_VAL));
        user.setPassword(prefs.getString("password", DEFAULT_VAL));
        user.setStatus(prefs.getString("status", DEFAULT_VAL));
        user.setVerify_status(prefs.getString("verified_status", DEFAULT_VAL));
        user.setPan_card_number(prefs.getString("pan_card_number", DEFAULT_VAL));
        user.setUidai_number(prefs.getString("uidai_number", DEFAULT_VAL));
        user.setFront_photo_id(prefs.getString("front_photo_id", DEFAULT_VAL));
        user.setBack_photo_id(prefs.getString("back_photo_id", DEFAULT_VAL));
        user.setShort_video(prefs.getString("short_video", DEFAULT_VAL));
        user.setPan_card_photo(prefs.getString("pan_card_photo", DEFAULT_VAL));
        user.setSignature_photo(prefs.getString("signature_photo", DEFAULT_VAL));
        user.setUser_avatar(prefs.getString("user_avatar", DEFAULT_VAL));
        user.setCreatedOn(prefs.getString("created_on", DEFAULT_VAL));
        user.setUserToken(prefs.getString("user_token", DEFAULT_VAL));
        user.setShareCode(prefs.getString("share_code", DEFAULT_VAL));
        user.setWalletTotal(prefs.getString("wallet_total", DEFAULT_VAL));
        user.setUser_donated(prefs.getString("user_donated", DEFAULT_VAL));
        user.setFollower_count(prefs.getString("follower_count", DEFAULT_VAL));
        user.setFollowing_count(prefs.getString("following_count", DEFAULT_VAL));

        return user;
    }

    public static void clearUser(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor dbEditor = prefs.edit();
        dbEditor.clear();
        dbEditor.commit();
    }
}
