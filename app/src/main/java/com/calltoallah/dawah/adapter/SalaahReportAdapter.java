package com.calltoallah.dawah.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.model.SalaahEvent;

import java.util.List;

public class SalaahReportAdapter extends RecyclerView.Adapter {

    private MainActivity context;
    private List<SalaahEvent> mItem;

    public SalaahReportAdapter(MainActivity context, List<SalaahEvent> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_salaah, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;
            viewHolders.txtSalaahDate.setText(mItem.get(position).getSalaahTimestamp());
            viewHolders.txtSalaahName.setText(mItem.get(position).getSalaahName());
            viewHolders.txtSalaahLocation.setText(mItem.get(position).getSalaahLocation());

            if (position % 2 == 0) {
                viewHolders.salaahContainer.setBackgroundColor(Color.parseColor("#F4F8F7"));
            } else {
                viewHolders.salaahContainer.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtSalaahDate, txtSalaahName, txtSalaahLocation;
        protected LinearLayout salaahContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            txtSalaahDate = itemView.findViewById(R.id.txtSalaahDate);
            txtSalaahName = itemView.findViewById(R.id.txtSalaahName);
            txtSalaahLocation = itemView.findViewById(R.id.txtSalaahLocation);
            salaahContainer = itemView.findViewById(R.id.salaahContainer);
        }
    }
}
