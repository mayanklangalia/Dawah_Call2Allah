package com.calltoallah.dawah.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.model.TasbihEvent;

import java.util.List;

public class TasbihReportAdapter extends RecyclerView.Adapter {

    private MainActivity context;
    private List<TasbihEvent> mItem;

    public TasbihReportAdapter(MainActivity context, List<TasbihEvent> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_tasbih, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;
            viewHolders.txtTasbihDate.setText(mItem.get(position).getTasbihTimestamp());
            viewHolders.txtTasbihCount.setText(mItem.get(position).getTasbihCount());
            viewHolders.txtTasbihName.setText(mItem.get(position).getTasbihName());

            if (position % 2 == 0) {
                viewHolders.tasbihContainer.setBackgroundColor(Color.parseColor("#F4F8F7"));
            } else {
                viewHolders.tasbihContainer.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtTasbihDate, txtTasbihCount, txtTasbihName;
        protected LinearLayout tasbihContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTasbihDate = itemView.findViewById(R.id.txtTasbihDate);
            txtTasbihCount = itemView.findViewById(R.id.txtTasbihCount);
            txtTasbihName = itemView.findViewById(R.id.txtTasbihName);
            tasbihContainer = itemView.findViewById(R.id.tasbihContainer);
        }
    }
}
