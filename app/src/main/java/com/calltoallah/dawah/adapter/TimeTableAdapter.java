package com.calltoallah.dawah.adapter;

import android.app.TimePickerDialog;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.model.TimeTableModel;

import java.util.Calendar;
import java.util.List;

public class TimeTableAdapter extends RecyclerView.Adapter {

    private MainActivity context;
    private List<TimeTableModel> mItem;

    public TimeTableAdapter(MainActivity context, List<TimeTableModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_timetable, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;

            final TimeTableModel timeTableModel = mItem.get(position);
            viewHolders.txtTTTime.setText(timeTableModel.getTt_time().trim());
            viewHolders.txtTTName.setText(timeTableModel.getTt_name().trim());

            if (position % 2 == 0) {
                viewHolders.ttContainer.setBackgroundColor(Color.parseColor("#F4F8F7"));
            } else {
                viewHolders.ttContainer.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }

            final Calendar calendar = Calendar.getInstance();
            viewHolders.imgAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.DatePickerTheme, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            boolean isPM = (hourOfDay >= 12);
                            String newTime = String.format("%02d:%02d %s", (hourOfDay == 12 || hourOfDay == 0) ? 12 : hourOfDay % 12, minute, isPM ? "PM" : "AM");
                            viewHolders.txtTTTime.setText(newTime);
                            timeTableModel.setTt_time(newTime);
                            notifyItemChanged(position, timeTableModel);
                        }
                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
                    timePickerDialog.show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtTTTime, txtTTName;
        protected LinearLayout ttContainer;
        protected ImageView imgAdd;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTTTime = itemView.findViewById(R.id.txtTTTime);
            txtTTName = itemView.findViewById(R.id.txtTTName);
            ttContainer = itemView.findViewById(R.id.ttContainer);
            imgAdd = itemView.findViewById(R.id.imgAdd);
        }
    }
}
