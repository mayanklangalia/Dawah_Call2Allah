package com.calltoallah.dawah.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.CircularImageView;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.fragment.MasjidDetailsFragment;
import com.calltoallah.dawah.model.FundListModel;
import com.calltoallah.dawah.model.MasjidModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;

import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class TopFundAdapter extends RecyclerView.Adapter {

    private MainActivity context;
    private List<FundListModel> mItem;

    public TopFundAdapter(MainActivity context, List<FundListModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_fund, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;

            final FundListModel fundListModel = mItem.get(position);
            viewHolders.txtDonorName.setText(fundListModel.getMasjidName());
            viewHolders.txtDonorUsername.setText(fundListModel.getMasjid_username());
            viewHolders.txtDonorAmt.setText("₹ " + fundListModel.getMasjidTotalAmount());

            if (fundListModel.getVerified_status().equalsIgnoreCase("1")) {
                viewHolders.txtDonorName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
                viewHolders.txtDonorName.setCompoundDrawablePadding(5);
            }

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.no_profile_image)
                    .error(R.drawable.no_profile_image);

            Glide.with(context)
                    .load(API_DOMAIN + fundListModel.getImage_path())
                    .apply(options)
                    .into(viewHolders.imgDonor);

            viewHolders.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getMasjidDetails(fundListModel.getId());
                }
            });
        }
    }

    private void getMasjidDetails(String masjidID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getMasjidDetails(masjidID);
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    try {
                        Gson gson = new Gson();
                        Reader reader = new StringReader(response.body().toString());
                        final MasjidModel masjidModel = gson.fromJson(reader, MasjidModel.class);

                        if (masjidModel.getMasjidListModel().size() > 0) {
                            Bundle bundle = new Bundle();
                            bundle.putParcelable(Constants.DATA, masjidModel.getMasjidListModel().get(0));

                            MasjidDetailsFragment masjidDetailsFragment = new MasjidDetailsFragment();
                            masjidDetailsFragment.setArguments(bundle);
                            context.loadFragment(masjidDetailsFragment);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtDonorName, txtDonorUsername, txtDonorAmt;
        protected CircularImageView imgDonor;

        public ViewHolder(View itemView) {
            super(itemView);
            txtDonorName = itemView.findViewById(R.id.txtDonorName);
            txtDonorUsername = itemView.findViewById(R.id.txtDonorUsername);
            txtDonorAmt = itemView.findViewById(R.id.txtDonorAmt);
            imgDonor = itemView.findViewById(R.id.imgDonor);
        }
    }
}
