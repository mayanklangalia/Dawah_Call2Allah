package com.calltoallah.dawah.adapter;

import android.content.res.ColorStateList;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.model.MasjidTransactionListModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MasjidTransactionAdapter extends RecyclerView.Adapter {

    private MainActivity context;
    private List<MasjidTransactionListModel> mItem;

    public MasjidTransactionAdapter(MainActivity context, List<MasjidTransactionListModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_masjid_transaction, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;

            final MasjidTransactionListModel masjidTransactionListModel = mItem.get(position);

            if (masjidTransactionListModel.getCustomOrderId() != null) {
                if (masjidTransactionListModel.getCustomOrderId().contains("DRT")) {
                    viewHolders.txtTransactionOrder.setText(Html.fromHtml("<b><font color="
                            + context.getResources().getColor(R.color.colorPrimaryLight) + "> "
                            + masjidTransactionListModel.getFull_name() + "</font></b>" + " Paid"));
                    viewHolders.txtTransactionAmount.setText("₹ " + masjidTransactionListModel.getAmount());
                    viewHolders.txtTransactionAmount.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorAccent)));

                } else if (masjidTransactionListModel.getCustomOrderId().contains("PST")) {
                    viewHolders.txtTransactionOrder.setText(Html.fromHtml("<b><font color="
                            + context.getResources().getColor(R.color.colorPrimaryLight) + "> "
                            + masjidTransactionListModel.getFull_name() + "</font></b>" + " Paid on Post " + masjidTransactionListModel.getPostId()));
                    viewHolders.txtTransactionAmount.setText("₹ " + masjidTransactionListModel.getAmount());
                    viewHolders.txtTransactionAmount.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorAccent)));

                } else if (masjidTransactionListModel.getCustomOrderId().contains("WDR")) {
                    viewHolders.txtTransactionOrder.setText("Funds Withdrawed to Local Bank");
                    viewHolders.txtTransactionAmount.setText("₹ " + masjidTransactionListModel.getAmount());
                    viewHolders.txtTransactionAmount.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorError)));
                }

            } else {
                viewHolders.txtTransactionOrder.setText("Payment Failure");
                viewHolders.txtTransactionAmount.setText("₹ " + masjidTransactionListModel.getAmount());
                viewHolders.txtTransactionAmount.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorYellow)));
            }

            if (masjidTransactionListModel.getDateTime() != null) {
                String inputPattern = "yyyy-MM-dd hh:mm:ss";
                String outputPattern = "dd MMM, yyyy hh:mm a";
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                try {
                    Date date = inputFormat.parse(masjidTransactionListModel.getDateTime());
                    String createdDate = outputFormat.format(date);
                    viewHolders.txtTransactionDate.setText(createdDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtTransactionOrder, txtTransactionDate, txtTransactionAmount;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTransactionOrder = itemView.findViewById(R.id.txtTransactionOrder);
            txtTransactionDate = itemView.findViewById(R.id.txtTransactionDate);
            txtTransactionAmount = itemView.findViewById(R.id.txtTransactionAmount);
        }
    }

    public int getWalletTotal(List<MasjidTransactionListModel> items) {
        int totalPrice = 0;
        for (MasjidTransactionListModel masjidTransactionListModel : items) {
            if (masjidTransactionListModel.getStatus().equalsIgnoreCase("1"))
                totalPrice += Integer.parseInt(masjidTransactionListModel.getAmount());
        }
        return totalPrice;
    }
}
