package com.calltoallah.dawah;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.HomeMasjidPostList;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.MainActivity.mBottomNavigationView;
import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class DonationActivity extends MasterActivity implements PaymentResultWithDataListener {

    private Bundle bundle;
    private int mode;
    private MasjidListModel masjidListModel;
    private HomeMasjidPostList homeMasjidPostList;
    private ImageView imgPMasjidIcon, imgAdd, imgRemove, mediaImage;
    private TextView txtToolbarTitle, txtPMasjidName, txtPMasjidDate, txtWalletAmount, txtPayableAmt, txtPGTax, mediaText;
    private EditText edtAmount;
    private CheckBox chkUseWallet;
    private Button btnDonationNow;
    private int counter = 0, userWalletAmt = 0, remainingWalletAmt = 0, payableAmt = 0;
    private double grossAmt = 0, zakatAmt = 0;
    private String masjidIdentifier = "";
    private String postIdentifier = "";
    private String orderIdentifier = "";
    private boolean useWallet = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_donation);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Razorpay Configuration ...
        Checkout.preload(getApplicationContext());

        Init();
        bundle = getIntent().getExtras();
        if (bundle != null) {
            mode = bundle.getInt(Constants.MODE);

            if (mode == 0) {
                homeMasjidPostList = bundle.getParcelable(Constants.DATA);
                txtToolbarTitle.setText(homeMasjidPostList.getMasjidName() + " Donation");

            } else if (mode == 1) {
                masjidListModel = bundle.getParcelable(Constants.DATA);
                txtToolbarTitle.setText(masjidListModel.getMasjidName() + " Donation");

            } else if (mode == 2) {
                zakatAmt = bundle.getDouble(Constants.DATA);
                txtToolbarTitle.setText("ZAKAT Donation");
            }
        }

        setData();
    }

    private void setData() {
        getWalletAmount();

        btnDonationNow.setText("PAY NOW");

        if (mode == 0) {
            txtPMasjidName.setText(homeMasjidPostList.getMasjidName());

            if (homeMasjidPostList.getCreatedOn() != null) {
                String inputPattern = "yyyy-MM-dd HH:mm:ss";
                String outputPattern = "EEEE, dd MMMM, yyyy";
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                try {
                    Date date = inputFormat.parse(homeMasjidPostList.getCreatedOn());
                    String createdDate = outputFormat.format(date);
                    txtPMasjidDate.setText(createdDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            Glide.with(DonationActivity.this)
                    .load(API_DOMAIN + homeMasjidPostList.getMasjidImage())
                    .into(imgPMasjidIcon);

            if (homeMasjidPostList.getDocPath().equalsIgnoreCase("")) {
                mediaImage.setVisibility(View.GONE);
                mediaText.setVisibility(View.VISIBLE);

                mediaText.setText(homeMasjidPostList.getPostText());
            } else {
                mediaImage.setVisibility(View.VISIBLE);
                mediaText.setVisibility(View.GONE);

                RequestOptions options = new RequestOptions()
                        .placeholder(R.drawable.no_profile_image)
                        .centerCrop()
                        .error(R.drawable.no_profile_image);

                Glide.with(DonationActivity.this)
                        .load(API_DOMAIN + homeMasjidPostList.getDocPath())
                        .apply(options)
                        .into(mediaImage);
            }

        } else if (mode == 1) {
            txtPMasjidName.setText(masjidListModel.getMasjidName());

            if (masjidListModel.getCreatedOn() != null) {
                String inputPattern = "yyyy-MM-dd HH:mm:ss";
                String outputPattern = "EEEE, dd MMMM, yyyy";
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                try {
                    Date date = inputFormat.parse(masjidListModel.getCreatedOn());
                    String createdDate = outputFormat.format(date);
                    txtPMasjidDate.setText(createdDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            Glide.with(DonationActivity.this)
                    .load(API_DOMAIN + masjidListModel.getImagePath())
                    .into(imgPMasjidIcon);

            mediaImage.setVisibility(View.VISIBLE);
            mediaText.setVisibility(View.GONE);
            Glide.with(DonationActivity.this)
                    .load(API_DOMAIN + masjidListModel.getImagePath())
                    .into(mediaImage);

        } else if (mode == 2) {
            imgAdd.setVisibility(View.GONE);
            imgRemove.setVisibility(View.GONE);
            txtPMasjidName.setText("Zakat Donation");

            counter = (int) zakatAmt;
            edtAmount.setText(counter + "");
            walletCalculation(chkUseWallet.isChecked());

            Calendar calendar = Calendar.getInstance();
            String outputPattern = "EEEE, dd MMMM, yyyy";
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
            txtPMasjidDate.setText(outputFormat.format(calendar.getTime()));

            Glide.with(DonationActivity.this)
                    .load(R.drawable.no_profile_image)
                    .into(imgPMasjidIcon);

            mediaImage.setVisibility(View.VISIBLE);
            mediaText.setVisibility(View.GONE);
            Glide.with(DonationActivity.this)
                    .load(R.drawable.ic_action_zakat)
                    .into(mediaImage);
        }

        btnDonationNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mode == 0) {
                    masjidIdentifier = homeMasjidPostList.getMasjidId();
                    postIdentifier = homeMasjidPostList.getId();

                } else if (mode == 1) {
                    masjidIdentifier = masjidListModel.getId();
                    postIdentifier = "0";

                } else if (mode == 2) {
                    masjidIdentifier = "0";
                    postIdentifier = "-1";
                }

                if (useWallet == true) {
                    int amount = Integer.parseInt(edtAmount.getText().toString());
                    if (amount <= userWalletAmt) {

                        if (mode == 0) {
                            orderIdentifier = "PST_" + "-" +
                                    homeMasjidPostList.getId() + "-" +
                                    UserPreferences.loadUser(DonationActivity.this).getId();

                        } else if (mode == 1) {
                            orderIdentifier = "DRT_" + "-" +
                                    masjidListModel.getId() + "-" +
                                    UserPreferences.loadUser(DonationActivity.this).getId();

                        } else if (mode == 2) {
                            orderIdentifier = "ZKT_" + "-" +
                                    UserPreferences.loadUser(DonationActivity.this).getId();
                        }

                        getWalletRequest(UserPreferences.loadUser(DonationActivity.this).getId(),
                                edtAmount.getText().toString(), postIdentifier, masjidIdentifier, orderIdentifier);

                    } else {
                        getOrderRequest(edtAmount.getText().toString(), String.valueOf(grossAmt),
                                "0", masjidIdentifier, String.valueOf(remainingWalletAmt),
                                UserPreferences.loadUser(DonationActivity.this).getId(), postIdentifier);
                    }

                } else {
                    getOrderRequest(edtAmount.getText().toString(), String.valueOf(grossAmt),
                            "0", masjidIdentifier, String.valueOf(remainingWalletAmt),
                            UserPreferences.loadUser(DonationActivity.this).getId(), postIdentifier);
                }


//                Toast.makeText(DonationActivity.this, "CheckValue: " + chkUseWallet.isChecked() + " \n "
//                        + "Amount: " + amount + " \n " +
//                        "Wallet Amount: " + remainingWalletAmt, Toast.LENGTH_SHORT).show();
//                Log.d("CheckValue:", chkUseWallet.isChecked() + " ");
//                Log.d("amount:", amount + " ");
//                Log.d("remainingWalletAmt:", remainingWalletAmt + " ");

//                if (chkUseWallet.isChecked()) {
//                    Toast.makeText(DonationActivity.this, "Check", Toast.LENGTH_SHORT).show();
//
//                } else {
//                    Toast.makeText(DonationActivity.this, "UnCheck", Toast.LENGTH_SHORT).show();
//                    Toast.makeText(DonationActivity.this, "RAZORPAY API CALL", Toast.LENGTH_SHORT).show();
//                }


//                Toast.makeText(DonationActivity.this, "Amount: " + amount, Toast.LENGTH_SHORT).show();
//                Toast.makeText(DonationActivity.this, "Wallet: " + remainingWalletAmt, Toast.LENGTH_SHORT).show();

//                if (chkUseWallet.isChecked()) {

//                } else {
//
//                }
            }
        });

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1;
                edtAmount.setText(counter + "");
            }
        });

        imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (counter > 0) {
                    counter = counter - 1;
                    edtAmount.setText(counter + "");
                }
            }
        });

        chkUseWallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                useWallet = isChecked;
                walletCalculation(isChecked);
            }
        });

        edtAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 0) {
                    counter = Integer.parseInt(s.toString());
                } else
                    counter = 0;

                walletCalculation(chkUseWallet.isChecked());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void getWalletRequest(String userID, String amount, String postIdentifier, String masjidIdentifier,
                                  String customOrderID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getPaymentUsingWallet(userID, amount,
                postIdentifier, masjidIdentifier, customOrderID);
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    Log.d("WALLET_SUCCESS: ", response.body().toString());

                    AlertDialog.Builder builder = new AlertDialog.Builder(DonationActivity.this);
                    builder.setTitle("Congratulations!")
                            .setMessage("We have received your transaction order. Please check your email for donation related details.")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

//                    try {
//                        JSONObject jsonObject = new JSONObject(response.body().toString());
//                        if (jsonObject.getString("status").equalsIgnoreCase("1") &&
//                                jsonObject.getString("payment_type").equalsIgnoreCase("gateway")) {
//                            String orderID = jsonObject.getString("order_id");
//                            startPayment(grossAmt, orderID);
//
//                        } else {
//                            Toast.makeText(DonationActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//                        }
//                    } catch (JSONException e) {
//                        Log.d("WALLET_EXCEPTION: ", e.getMessage().toString());
//                        Toast.makeText(DonationActivity.this, "Exception " + e.getMessage().toString(), Toast.LENGTH_SHORT).show();
//                        e.printStackTrace();
//                    }

                } else {
                    Log.d("WALLET_FAIL: ", response.toString());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("WALLET_FAILURE: ", t.getMessage().toString());
            }
        });
    }

    private void getOrderRequest(String amount, String grossAmount, String useWallet, String masjidIdentifier, String remainingAmount,
                                 String userID, String postIdentifier) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getOrder(amount, grossAmount,
                useWallet, masjidIdentifier, remainingAmount, userID, postIdentifier);
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    Log.d("ORDER_SUCCESS: ", response.body().toString());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        if (jsonObject.getString("status").equalsIgnoreCase("1") &&
                                jsonObject.getString("payment_type").equalsIgnoreCase("gateway")) {
                            String orderID = jsonObject.getString("order_id");
                            startPayment(grossAmt, orderID);

                        } else {
                            Toast.makeText(DonationActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        Log.d("ORDER_EXCEPTION: ", e.getMessage().toString());
                        Toast.makeText(DonationActivity.this, "Exception " + e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                } else {
                    Log.d("ORDER_FAIL: ", response.toString());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("ORDER_FAILURE: ", t.getMessage().toString());
            }
        });
    }

    private void getWalletAmount() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getUserWallet(UserPreferences.loadUser(DonationActivity.this).getId());
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        Log.d("UWALLET: ", response.body().toString());

                        if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                            userWalletAmt = Integer.parseInt(jsonObject.getString("amount"));

                            if (userWalletAmt > 0) {
                                txtWalletAmount.setText(userWalletAmt + "");
                            } else {
                                txtWalletAmount.setText("0");
                            }

                        } else {
                            Toast.makeText(DonationActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    private void Init() {
        txtToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        imgPMasjidIcon = (ImageView) findViewById(R.id.imgPMasjidIcon);
        txtPMasjidName = (TextView) findViewById(R.id.txtPMasjidName);
        txtPMasjidDate = (TextView) findViewById(R.id.txtPMasjidDate);

        imgAdd = (ImageView) findViewById(R.id.imgAdd);
        edtAmount = (EditText) findViewById(R.id.edtAmount);
        imgRemove = (ImageView) findViewById(R.id.imgRemove);

        txtWalletAmount = (TextView) findViewById(R.id.txtWalletAmount);
        chkUseWallet = (CheckBox) findViewById(R.id.chkUseWallet);
        btnDonationNow = (Button) findViewById(R.id.btnDonationNow);
        txtPayableAmt = (TextView) findViewById(R.id.txtPayableAmt);
        txtPGTax = (TextView) findViewById(R.id.txtPGTax);

        mediaImage = (ImageView) findViewById(R.id.mediaImage);
        mediaText = (TextView) findViewById(R.id.mediaText);
    }

    private void startPayment(double amount, String orderID) {
        double grossAmount = amount * 100;
        final Checkout co = new Checkout();
        co.setImage(R.drawable.ic_action_logo);

        try {
            JSONObject options = new JSONObject();
            options.put("name", getString(R.string.app_name));
            options.put("description", "Donation Charges");
            options.put("amount", grossAmount);
            options.put("payment_capture", true);
            options.put("currency", "INR");
            options.put("order_id", orderID);

            JSONObject preFill = new JSONObject();
            preFill.put("email", UserPreferences.loadUser(DonationActivity.this).getEmail());
            preFill.put("contact", UserPreferences.loadUser(DonationActivity.this).getPhoneNumber());
            options.put("prefill", preFill);

            co.open(DonationActivity.this, options);
        } catch (Exception e) {
            Log.d("Error: ", e.getMessage().toString());
            e.printStackTrace();
        }
    }

    public void walletCalculation(boolean isChecked) {
        if (isChecked) {
            int temp_amount = userWalletAmt - counter;
            if (temp_amount < 0) {
                remainingWalletAmt = 0;
                payableAmt = -(temp_amount);

            } else {
                remainingWalletAmt = temp_amount;
                payableAmt = 0;
            }


        } else {
            payableAmt = counter;
            remainingWalletAmt = userWalletAmt;
        }

        double calculateGross = (payableAmt * 5) / 100;
        DecimalFormat df = new DecimalFormat("#.##");
        txtPGTax.setText(df.format(calculateGross));
        txtPayableAmt.setText(df.format(payableAmt));

        if ((calculateGross + payableAmt) == 0.0) {
            btnDonationNow.setText("PAY NOW");
        } else {
            btnDonationNow.setText("PAY " + (calculateGross + payableAmt) + " NOW");
        }

        grossAmt = (calculateGross + payableAmt);
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID, PaymentData paymentData) {
        try {
            Log.d("onPaymentSuccess: ", razorpayPaymentID + " - "
                    + "OrderID: " + paymentData.getOrderId()
                    + " PaymentID: " + paymentData.getPaymentId()
                    + " Signature: " + paymentData.getSignature());
//            finish();

            String use_wallet = "0";
            if (chkUseWallet.isChecked())
                use_wallet = "1";
            else
                use_wallet = "0";

            if (mode == 0) {
                orderIdentifier = "PST" + "-" +
                        homeMasjidPostList.getId() + "-" +
                        UserPreferences.loadUser(DonationActivity.this).getId() + "-" +
                        paymentData.getPaymentId();

            } else if (mode == 1) {
                orderIdentifier = "DRT" + "-" +
                        masjidListModel.getId() + "-" +
                        UserPreferences.loadUser(DonationActivity.this).getId() + "-" +
                        paymentData.getPaymentId();

            } else if (mode == 2) {
                orderIdentifier = "ZKT" + "-" +
                        UserPreferences.loadUser(DonationActivity.this).getId() + "-" +
                        paymentData.getPaymentId();
            }

            paymentVerification(paymentData.getPaymentId(), paymentData.getOrderId(),
                    paymentData.getSignature(), use_wallet, UserPreferences.loadUser(DonationActivity.this).getId(), postIdentifier, orderIdentifier);

        } catch (Exception e) {
            Log.d("onPaymentError: ", e.getMessage().toString());
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentError(int code, String response, PaymentData paymentData) {
        try {
            Toast.makeText(this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.d("onPaymentError", "Exception in onPaymentError", e);
        }
    }

    private void paymentVerification(String paymentID, String orderID,
                                     String signature, String wallet, String userID, String postID, String customOrderID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.addPaymentVerification
                (paymentID, orderID, signature, wallet, userID, postID, customOrderID);

        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("PaymentResponse: ", response.body().toString());

                if (response.isSuccessful()) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(DonationActivity.this);
                    builder.setTitle("Congratulations!")
                            .setMessage("We have received your transaction order. Please check your email for donation related details.")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    Toast.makeText(DonationActivity.this, "Payment Failed", Toast.LENGTH_SHORT).show();
//                    Log.d("PaymentFail: ", response.toString() + " " + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("PaymentFailure: ", t.getMessage().toString() + " " + t.getCause().getMessage().toString());
                Toast.makeText(DonationActivity.this, "" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mode == 0) {
            mBottomNavigationView.setSelectedItemId(R.id.nav_item_1);
            finish();

        } else if (mode == 1) {
            mBottomNavigationView.setSelectedItemId(R.id.nav_item_2);
            finish();

        } else if (mode == 2) {
            mBottomNavigationView.setSelectedItemId(R.id.nav_item_1);
            finish();
        }
    }
}

