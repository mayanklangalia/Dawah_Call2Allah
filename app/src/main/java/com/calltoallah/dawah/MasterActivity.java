package com.calltoallah.dawah;

import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatActivity;

import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.model.QuranDataModel;
import com.calltoallah.dawah.model.TimeTableModel;

import java.util.ArrayList;
import java.util.List;

public class MasterActivity extends AppCompatActivity {

    public static List<QuranDataModel> mQuranList = new ArrayList<>();
    public static ArrayList<String> mNamazTimeList = new ArrayList<>();
    public static String userID;

    public void saveLogin(String key, int value) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    public int loadLogin() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        int token = sp.getInt(Constants.LOGIN, 0);
        return token;
    }
}
