package com.calltoallah.dawah;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.comman.CircularImageView;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.LoginModel;
import com.calltoallah.dawah.model.ProfileModel;
import com.calltoallah.dawah.model.ProfileUserInfo;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends MasterActivity implements GoogleApiClient.OnConnectionFailedListener {

    private Button btnLogin;
    private TextView txtSignUp, txtForgotPassword;
    private EditText txtUsername, txtPassword;
    private ImageView imgGoogleLogin;
    private boolean responseFailed = false;
    private ProgressbarManager progressbarManager;

    // For Google Login ...
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;
    private ImageView mGoogleLogin;
    private int RC_SIGN_IN = 100;
    private String regId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                regId = instanceIdResult.getToken();
                Log.d("FirebaseApp Token: ", regId);
            }
        });

        progressbarManager = new ProgressbarManager(LoginActivity.this);

        Init();
        configureGoogleInfo();
        setData();
    }

    private void setData() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtUsername.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(LoginActivity.this, "Please enter username!", Toast.LENGTH_SHORT).show();
                } else if (txtPassword.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(LoginActivity.this, "Please enter password!", Toast.LENGTH_SHORT).show();
                } else {
                    progressbarManager.show();
                    UserLoginRequest();
                }
            }
        });

        imgGoogleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });
    }

    private void UserLoginRequest() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.getUserLogin(
                txtUsername.getText().toString(), txtPassword.getText().toString(), regId);
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    progressbarManager.dismiss();

                    try {
                        String res = response.body().string();
                        Log.d("login: ", res);

                        try {
//                            Type listType = new TypeToken<ArrayList<LoginModel>>() {
//                            }.getType();
//                            ArrayList<LoginModel> workList = new Gson().fromJson(res, listType);

                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            LoginModel userModel = gson.fromJson(reader, LoginModel.class);

                            if (userModel.getStatus().equalsIgnoreCase("1")) {
                                getProfile(userModel.getUserId());

                            } else {
                                Toast.makeText(LoginActivity.this, "" + userModel.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {

                            progressbarManager.dismiss();
                            Toast.makeText(LoginActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    progressbarManager.dismiss();
                    Toast.makeText(LoginActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressbarManager.dismiss();
                Toast.makeText(LoginActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getProfile(String userId) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.getProfileInfo(userId);
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();
                        Log.d("Profile: ", res);

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                            if (profileModel.getProfileUserInfo().size() > 0) {
                                ProfileUserInfo profileUserInfo = profileModel.getProfileUserInfo().get(0);
                                UserPreferences.saveUser(LoginActivity.this, profileUserInfo);

                                Toast.makeText(LoginActivity.this, "You have logged in successfully", Toast.LENGTH_SHORT).show();
                                saveLogin(Constants.LOGIN, 1);

                                startActivity(new Intent(LoginActivity.this, MasjidIntroActivity.class));
                                finish();
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(LoginActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(LoginActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(LoginActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void GoogleLoginRequest(GoogleSignInAccount googleSignInAccount, String referralCode) {
        String googleImage = googleSignInAccount.getPhotoUrl() != null ? googleSignInAccount.getPhotoUrl().toString() : null;
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        final Call<String> apiCall = apiInterface.getGoogleLogin(
                googleSignInAccount.getEmail(),
                googleSignInAccount.getDisplayName(), referralCode, googleImage);
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    progressbarManager.dismiss();

                    try {
                        String res = response.body().toString();
                        Log.d("Google", "onResponse: " + res);

                        try {
//                            Type listType = new TypeToken<ArrayList<LoginModel>>() {
//                            }.getType();
//                            ArrayList<LoginModel> workList = new Gson().fromJson(res, listType);

                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            LoginModel userModel = gson.fromJson(reader, LoginModel.class);

                            if (userModel.getStatus().equalsIgnoreCase("1")) {
                                if (userModel.getAlready_status().equalsIgnoreCase("1")) {
                                    showGoogleDialog(googleSignInAccount);
                                } else {
                                    getProfile(userModel.getUserId());
                                }

                            } else {
                                Toast.makeText(LoginActivity.this, "" + userModel.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            progressbarManager.dismiss();
                            Toast.makeText(LoginActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        Log.d("Google_Exception: ", response.body().toString());
                        e.printStackTrace();
                    }

                } else {
                    Log.d("Google_Else: ", response.body().toString());
                    progressbarManager.dismiss();
                    Toast.makeText(LoginActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("Google_onFailure: ", t.getMessage().toString());

                progressbarManager.dismiss();
                Toast.makeText(LoginActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Init() {
        btnLogin = (Button) findViewById(R.id.btnLogin);

        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);

        txtSignUp = (TextView) findViewById(R.id.txtSignUp);
        txtForgotPassword = (TextView) findViewById(R.id.txtForgotPassword);

        imgGoogleLogin = (ImageView) findViewById(R.id.imgGoogleLogin);
    }

    private void configureGoogleInfo() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Initializing google api client......
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(LoginActivity.this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    //This function will option signing intent
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    //After the signing we are calling this function
    private void handleSignInResult(final GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.d("GPHOTO: ", acct.getPhotoUrl() != null ? acct.getPhotoUrl().toString() : null + "");
            GoogleLoginRequest(acct, "");

        } else {
            Toast.makeText(this, "Google Login Failed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void showGoogleDialog(GoogleSignInAccount acct) {
        View parentView = getLayoutInflater().inflate(R.layout.dialog_refercode, null);
        BottomSheetDialog dialog = new BottomSheetDialog(LoginActivity.this);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialog.setContentView(parentView);

        CircularImageView googlePhoto = dialog.findViewById(R.id.googlePhoto);
        TextView googleName = dialog.findViewById(R.id.googleName);
        TextView googleEmail = dialog.findViewById(R.id.googleEmail);
        EditText enterCode = dialog.findViewById(R.id.enterCode);
        ImageView imgSkip = dialog.findViewById(R.id.imgSkip);
        ImageView imgOK = dialog.findViewById(R.id.imgOK);

        googleName.setText(acct.getDisplayName());
        googleEmail.setText(acct.getEmail());

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.no_profile_image)
                .error(R.drawable.no_profile_image);

        Glide.with(LoginActivity.this)
                .load(acct.getPhotoUrl())
                .apply(options)
                .into(googlePhoto);

        imgOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enterCode.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(LoginActivity.this, "Enter referral code?", Toast.LENGTH_SHORT).show();
                } else {
                    GoogleLoginRequest(acct, enterCode.getText().toString());
                    dialog.dismiss();
                }
            }
        });

        imgSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoogleLoginRequest(acct, "");
                dialog.dismiss();
            }
        });

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog dialogc = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = dialogc.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        super.onActivityResult(requestCode, responseCode, data);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection Timeout.", Toast.LENGTH_SHORT).show();
    }
}
