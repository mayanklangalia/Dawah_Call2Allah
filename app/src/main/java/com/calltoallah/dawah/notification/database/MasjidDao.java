package com.calltoallah.dawah.notification.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface MasjidDao {
    @Query("SELECT * FROM MasjidMaster")
    List<MasjidMaster> getAll();

    @Query("SELECT * FROM MasjidMaster where latitude=:lat AND longitude=:lang limit 1")
    MasjidMaster getMasjidFromLocation(double lat, double lang);

    @Query("SELECT * FROM MasjidMaster where uid=:id")
    MasjidMaster getMasjidFromId(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addData(MasjidMaster data);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addData(List<MasjidMaster> data);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateData(MasjidMaster... data);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updateData(List<MasjidMaster> data);

    @Query("DELETE FROM MasjidMaster")
    void deleteData();
}
