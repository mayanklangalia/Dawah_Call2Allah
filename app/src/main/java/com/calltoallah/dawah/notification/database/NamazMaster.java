package com.calltoallah.dawah.notification.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.text.SimpleDateFormat;

@Entity(tableName = "NamazMaster")
public class NamazMaster implements Serializable {

    @Ignore
    public MasjidMaster masjidMaster;

    @PrimaryKey(autoGenerate = true)
    public long uid;

    @ColumnInfo(name="masjid_id")
    public long masjid_id;

    @ColumnInfo(name = "name")
    public String namaz_name;

    @ColumnInfo(name = "namaz_time")
    public long namaz_time;

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return "NamazMaster{" +
                "uid=" + uid +
                ", masjid_id=" + masjid_id +
                ", name='" + namaz_name + '\'' +
                ", namaz_time=" + namaz_time + "(" + simpleDateFormat.format(namaz_time) + ")" +
                '}';
    }
}
