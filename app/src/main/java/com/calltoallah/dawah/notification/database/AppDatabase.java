package com.calltoallah.dawah.notification.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {MasjidMaster.class, NamazMaster.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract MasjidDao masjidDao();
    public abstract NamazDao namazDao();
}
