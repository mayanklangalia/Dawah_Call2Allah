package com.calltoallah.dawah;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends MasterActivity {

    private EditText edtEmail;
    private Button btnSubmit;
    private ProgressbarManager progressbarManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        Init();
        setData();
    }

    private void setData() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtEmail.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(ForgotPasswordActivity.this, "Email should not be blank!", Toast.LENGTH_SHORT).show();

                } else {
                    progressbarManager.show();
                    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                    final Call<String> loginCall = apiInterface.getForgotPassword(
                            edtEmail.getText().toString());
                    loginCall.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            progressbarManager.dismiss();
                            Log.d("FP: ", response.body().toString());

                            if (response.isSuccessful()) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response.body().toString());
                                    String status = jsonObject.getString("status");
                                    String message = jsonObject.getString("message");

                                    if (status.equalsIgnoreCase("1")) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(ForgotPasswordActivity.this);
                                        builder.setTitle("Password Updated!")
                                                .setMessage("Your password has been sent to your registered email address!")
                                                .setCancelable(false)
                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        startActivity(new Intent(ForgotPasswordActivity.this, LoginActivity.class));
                                                        finish();
                                                    }
                                                });
                                        AlertDialog dialog = builder.create();
                                        dialog.show();

                                    } else {
                                        Toast.makeText(ForgotPasswordActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } else {
                                Toast.makeText(ForgotPasswordActivity.this, "Your email does not exist!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                        }
                    });
                }
            }
        });
    }

    private void Init() {
        progressbarManager = new ProgressbarManager(ForgotPasswordActivity.this);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
    }
}
