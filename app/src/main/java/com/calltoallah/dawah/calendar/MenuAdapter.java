package com.calltoallah.dawah.calendar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.R;
import com.calltoallah.dawah.model.MenuModel;

import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MyViewHolder> {
    private Context context;
    private List<MenuModel> mList;
    private OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public List<MenuModel> getListModel(int position) {
        return mList;
    }

    public List<MenuModel> getList() {
        return mList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        protected TextView menuTitle;
        protected ImageView menuIcon;
        protected LinearLayout menuParent;

        public MyViewHolder(View view) {
            super(view);
            menuParent = (LinearLayout) view.findViewById(R.id.menuParent);
            menuIcon = (ImageView) view.findViewById(R.id.menuIcon);
            menuTitle = (TextView) view.findViewById(R.id.menuTitle);
        }
    }

    public MenuAdapter(Context context, List<MenuModel> list) {
        this.context = context;
        this.mList = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_menu, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.menuTitle.setText(mList.get(position).getMenuTitle());
        holder.menuIcon.setImageResource(mList.get(position).getMenuIcon());
        holder.menuParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(view, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
