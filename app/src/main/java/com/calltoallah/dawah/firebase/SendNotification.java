package com.calltoallah.dawah.firebase;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendNotification {

    public static void getUserNotificationToken(String userid, String title, String message, Context context) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("Tokens").child(userid).child("token").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                sendNotifications(snapshot.getValue(String.class), title, message, context);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w("TAG", "onCancelled", error.toException());
            }
        });
    }

    public static void sendNotifications(String usertoken, String title, String message, Context context) {
        Log.i("TAG", "notification_error: " + usertoken);
        Data data = new Data(title, message);

        NotificationSender sender = new NotificationSender(data, usertoken);
        APIInterface apiCalls = APIClient.getFireBaseClient().create(APIInterface.class);
        apiCalls.sendNotifcation(sender).enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                Log.d("Notification: ", "Success: " + response.body().toString());
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                Log.d("NotificationOnFailure:", "notification_error: " + t.getMessage().toString());
            }
        });
    }
}
