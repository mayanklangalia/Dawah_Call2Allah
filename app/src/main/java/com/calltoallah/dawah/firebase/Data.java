package com.calltoallah.dawah.firebase;

public class Data {
    private String title;
    private String body;

    public Data(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public Data() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return body;
    }

    public void setMessage(String body) {
        this.body = body;
    }

}