package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalendarEventListModel implements Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("event_date")
    @Expose
    private String eventDate;
    @SerializedName("user_id")
    @Expose
    private String userId;

    private int eventDay;
    private int eventMonth;
    private int eventYear;

    public int getEventDay() {
        return eventDay;
    }

    public void setEventDay(int eventDay) {
        this.eventDay = eventDay;
    }

    public int getEventMonth() {
        return eventMonth;
    }

    public void setEventMonth(int eventMonth) {
        this.eventMonth = eventMonth;
    }

    public int getEventYear() {
        return eventYear;
    }

    public void setEventYear(int eventYear) {
        this.eventYear = eventYear;
    }

    public final static Parcelable.Creator<CalendarEventListModel> CREATOR = new Creator<CalendarEventListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CalendarEventListModel createFromParcel(Parcel in) {
            return new CalendarEventListModel(in);
        }

        public CalendarEventListModel[] newArray(int size) {
            return (new CalendarEventListModel[size]);
        }

    }
            ;

    protected CalendarEventListModel(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.eventName = ((String) in.readValue((String.class.getClassLoader())));
        this.eventDate = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CalendarEventListModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(eventName);
        dest.writeValue(eventDate);
        dest.writeValue(userId);
    }

    public int describeContents() {
        return 0;
    }

}
