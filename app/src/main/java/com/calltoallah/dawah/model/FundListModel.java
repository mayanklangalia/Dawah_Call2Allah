package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FundListModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("masjid_total_amount")
    @Expose
    private String masjidTotalAmount;
    @SerializedName("masjid_name")
    @Expose
    private String masjidName;
    @SerializedName("verified_status")
    @Expose
    private String verified_status;
    @SerializedName("masjid_username")
    @Expose
    private String masjid_username;
    @SerializedName("image_path")
    @Expose
    private String image_path;

    public final static Parcelable.Creator<FundListModel> CREATOR = new Creator<FundListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FundListModel createFromParcel(Parcel in) {
            return new FundListModel(in);
        }

        public FundListModel[] newArray(int size) {
            return (new FundListModel[size]);
        }

    };

    protected FundListModel(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidTotalAmount = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidName = ((String) in.readValue((String.class.getClassLoader())));
        this.masjid_username = ((String) in.readValue((String.class.getClassLoader())));
        this.verified_status = ((String) in.readValue((String.class.getClassLoader())));
        this.image_path = ((String) in.readValue((String.class.getClassLoader())));
    }

    public FundListModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMasjidTotalAmount() {
        return masjidTotalAmount;
    }

    public void setMasjidTotalAmount(String masjidTotalAmount) {
        this.masjidTotalAmount = masjidTotalAmount;
    }

    public String getMasjidName() {
        return masjidName;
    }

    public void setMasjidName(String masjidName) {
        this.masjidName = masjidName;
    }

    public String getVerified_status() {
        return verified_status;
    }

    public void setVerified_status(String verified_status) {
        this.verified_status = verified_status;
    }

    public String getMasjid_username() {
        return masjid_username;
    }

    public void setMasjid_username(String masjid_username) {
        this.masjid_username = masjid_username;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(masjidTotalAmount);
        dest.writeValue(masjidName);
        dest.writeValue(masjid_username);
        dest.writeValue(verified_status);
        dest.writeValue(image_path);

    }

    public int describeContents() {
        return 0;
    }

}