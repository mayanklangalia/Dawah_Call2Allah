package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllUserModel implements Parcelable {

    @SerializedName("user_list")
    @Expose
    private List<ProfileUserInfo> userList = null;
    public final static Parcelable.Creator<AllUserModel> CREATOR = new Creator<AllUserModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AllUserModel createFromParcel(Parcel in) {
            return new AllUserModel(in);
        }

        public AllUserModel[] newArray(int size) {
            return (new AllUserModel[size]);
        }

    };

    protected AllUserModel(Parcel in) {
        in.readList(this.userList, (ProfileUserInfo.class.getClassLoader()));
    }

    public AllUserModel() {
    }

    public List<ProfileUserInfo> getUserList() {
        return userList;
    }

    public void setUserList(List<ProfileUserInfo> userList) {
        this.userList = userList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(userList);
    }

    public int describeContents() {
        return 0;
    }
}
