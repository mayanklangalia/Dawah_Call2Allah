package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuranModel implements Parcelable {

    @SerializedName("quran")
    @Expose
    private List<QuranDataModel> quranDataModel = null;
    public final static Creator<QuranModel> CREATOR = new Creator<QuranModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public QuranModel createFromParcel(Parcel in) {
            return new QuranModel(in);
        }

        public QuranModel[] newArray(int size) {
            return (new QuranModel[size]);
        }

    };

    protected QuranModel(Parcel in) {
        in.readList(this.quranDataModel, (QuranDataModel.class.getClassLoader()));
    }

    public QuranModel() {
    }

    public List<QuranDataModel> getQuranDataModel() {
        return quranDataModel;
    }

    public void setQuranDataModel(List<QuranDataModel> quranDataModel) {
        this.quranDataModel = quranDataModel;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(quranDataModel);
    }

    public int describeContents() {
        return 0;
    }

}