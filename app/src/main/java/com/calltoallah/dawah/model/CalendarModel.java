package com.calltoallah.dawah.model;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalendarModel implements Parcelable
{

    @SerializedName("event_list")
    @Expose
    private List<CalendarEventListModel> calendarEventListModel = null;
    public final static Parcelable.Creator<CalendarModel> CREATOR = new Creator<CalendarModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CalendarModel createFromParcel(Parcel in) {
            return new CalendarModel(in);
        }

        public CalendarModel[] newArray(int size) {
            return (new CalendarModel[size]);
        }

    }
            ;

    protected CalendarModel(Parcel in) {
        in.readList(this.calendarEventListModel, (CalendarEventListModel.class.getClassLoader()));
    }

    public CalendarModel() {
    }

    public List<CalendarEventListModel> getCalendarEventListModel() {
        return calendarEventListModel;
    }

    public void setCalendarEventListModel(List<CalendarEventListModel> calendarEventListModel) {
        this.calendarEventListModel = calendarEventListModel;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(calendarEventListModel);
    }

    public int describeContents() {
        return 0;
    }

}