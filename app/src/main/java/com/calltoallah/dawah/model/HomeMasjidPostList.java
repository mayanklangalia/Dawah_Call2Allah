package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeMasjidPostList extends RecyclerViewItem implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("masjid_id")
    @Expose
    private String masjidId;
    @SerializedName("post_text")
    @Expose
    private String postText;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("doc_path")
    @Expose
    private String docPath;
    @SerializedName("donation_button")
    @Expose
    private String donationButton;
    @SerializedName("asked_donation_amount")
    @Expose
    private String asked_donation_amount;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("masjid_name")
    @Expose
    private String masjidName;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("masjid_image")
    @Expose
    private String masjidImage;
    @SerializedName("liked_post")
    @Expose
    private String likedPost;
    @SerializedName("post_liked")
    @Expose
    private boolean post_liked;
    @SerializedName("like_count")
    @Expose
    private String like_count;
    @SerializedName("share_count")
    @Expose
    private String share_count;
    @SerializedName("user_donated")
    @Expose
    private String user_donated;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("verified_status")
    @Expose
    private String verified_status;
    @SerializedName("qr_code")
    @Expose
    private String qr_code;
    @SerializedName("masjid_username")
    @Expose
    private String masjid_username;
    @SerializedName("user_id")
    @Expose
    private String user_id;

    protected HomeMasjidPostList(Parcel in) {
        id = in.readString();
        masjidId = in.readString();
        postText = in.readString();
        postType = in.readString();
        docPath = in.readString();
        donationButton = in.readString();
        asked_donation_amount = in.readString();
        createdOn = in.readString();
        masjidName = in.readString();
        createdDate = in.readString();
        address = in.readString();
        masjidImage = in.readString();
        likedPost = in.readString();
        post_liked = in.readByte() != 0;
        like_count = in.readString();
        share_count = in.readString();
        user_donated = in.readString();
        status = in.readString();
        verified_status = in.readString();
        qr_code = in.readString();
        masjid_username = in.readString();
        user_id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(masjidId);
        dest.writeString(postText);
        dest.writeString(postType);
        dest.writeString(docPath);
        dest.writeString(donationButton);
        dest.writeString(asked_donation_amount);
        dest.writeString(createdOn);
        dest.writeString(masjidName);
        dest.writeString(createdDate);
        dest.writeString(address);
        dest.writeString(masjidImage);
        dest.writeString(likedPost);
        dest.writeByte((byte) (post_liked ? 1 : 0));
        dest.writeString(like_count);
        dest.writeString(share_count);
        dest.writeString(user_donated);
        dest.writeString(status);
        dest.writeString(verified_status);
        dest.writeString(qr_code);
        dest.writeString(masjid_username);
        dest.writeString(user_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HomeMasjidPostList> CREATOR = new Creator<HomeMasjidPostList>() {
        @Override
        public HomeMasjidPostList createFromParcel(Parcel in) {
            return new HomeMasjidPostList(in);
        }

        @Override
        public HomeMasjidPostList[] newArray(int size) {
            return new HomeMasjidPostList[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMasjidId() {
        return masjidId;
    }

    public void setMasjidId(String masjidId) {
        this.masjidId = masjidId;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getDocPath() {
        return docPath;
    }

    public void setDocPath(String docPath) {
        this.docPath = docPath;
    }

    public String getDonationButton() {
        return donationButton;
    }

    public void setDonationButton(String donationButton) {
        this.donationButton = donationButton;
    }

    public String getAsked_donation_amount() {
        return asked_donation_amount;
    }

    public void setAsked_donation_amount(String asked_donation_amount) {
        this.asked_donation_amount = asked_donation_amount;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getMasjidName() {
        return masjidName;
    }

    public void setMasjidName(String masjidName) {
        this.masjidName = masjidName;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMasjidImage() {
        return masjidImage;
    }

    public void setMasjidImage(String masjidImage) {
        this.masjidImage = masjidImage;
    }

    public String getLikedPost() {
        return likedPost;
    }

    public void setLikedPost(String likedPost) {
        this.likedPost = likedPost;
    }

    public boolean isPost_liked() {
        return post_liked;
    }

    public void setPost_liked(boolean post_liked) {
        this.post_liked = post_liked;
    }

    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public String getShare_count() {
        return share_count;
    }

    public void setShare_count(String share_count) {
        this.share_count = share_count;
    }

    public String getUser_donated() {
        return user_donated;
    }

    public void setUser_donated(String user_donated) {
        this.user_donated = user_donated;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVerified_status() {
        return verified_status;
    }

    public void setVerified_status(String verified_status) {
        this.verified_status = verified_status;
    }

    public String getQr_code() {
        return qr_code;
    }

    public void setQr_code(String qr_code) {
        this.qr_code = qr_code;
    }

    public String getMasjid_username() {
        return masjid_username;
    }

    public void setMasjid_username(String masjid_username) {
        this.masjid_username = masjid_username;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

}