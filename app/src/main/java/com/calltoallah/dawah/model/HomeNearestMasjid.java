package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeNearestMasjid implements Parcelable {

    @SerializedName("masjid_name")
    @Expose
    private String masjidName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("masjid_image")
    @Expose
    private String masjidImage;
    @SerializedName("name_of_namez")
    @Expose
    private String nameOfNamez;
    @SerializedName("timing_of_masjid")
    @Expose
    private String timingOfMasjid;
    @SerializedName("ids_of_prayer")
    @Expose
    private String idsOfPrayer;
    @SerializedName("distance")
    @Expose
    private String distance;
    public final static Creator<HomeNearestMasjid> CREATOR = new Creator<HomeNearestMasjid>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HomeNearestMasjid createFromParcel(Parcel in) {
            return new HomeNearestMasjid(in);
        }

        public HomeNearestMasjid[] newArray(int size) {
            return (new HomeNearestMasjid[size]);
        }

    };

    protected HomeNearestMasjid(Parcel in) {
        this.masjidName = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidImage = ((String) in.readValue((String.class.getClassLoader())));
        this.nameOfNamez = ((String) in.readValue((String.class.getClassLoader())));
        this.timingOfMasjid = ((String) in.readValue((String.class.getClassLoader())));
        this.idsOfPrayer = ((String) in.readValue((String.class.getClassLoader())));
        this.distance = ((String) in.readValue((String.class.getClassLoader())));
    }

    public HomeNearestMasjid() {
    }

    public String getMasjidName() {
        return masjidName;
    }

    public void setMasjidName(String masjidName) {
        this.masjidName = masjidName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMasjidImage() {
        return masjidImage;
    }

    public void setMasjidImage(String masjidImage) {
        this.masjidImage = masjidImage;
    }

    public String getNameOfNamez() {
        return nameOfNamez;
    }

    public void setNameOfNamez(String nameOfNamez) {
        this.nameOfNamez = nameOfNamez;
    }

    public String getTimingOfMasjid() {
        return timingOfMasjid;
    }

    public void setTimingOfMasjid(String timingOfMasjid) {
        this.timingOfMasjid = timingOfMasjid;
    }

    public String getIdsOfPrayer() {
        return idsOfPrayer;
    }

    public void setIdsOfPrayer(String idsOfPrayer) {
        this.idsOfPrayer = idsOfPrayer;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(masjidName);
        dest.writeValue(address);
        dest.writeValue(masjidImage);
        dest.writeValue(nameOfNamez);
        dest.writeValue(timingOfMasjid);
        dest.writeValue(idsOfPrayer);
        dest.writeValue(distance);
    }

    public int describeContents() {
        return 0;
    }

}