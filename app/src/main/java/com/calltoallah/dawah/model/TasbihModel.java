package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TasbihModel implements Parcelable {

    @SerializedName("tasbih_events")
    @Expose
    private List<TasbihEvent> tasbihEvents = null;
    public final static Creator<TasbihModel> CREATOR = new Creator<TasbihModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public TasbihModel createFromParcel(Parcel in) {
            return new TasbihModel(in);
        }

        public TasbihModel[] newArray(int size) {
            return (new TasbihModel[size]);
        }

    };

    protected TasbihModel(Parcel in) {
        in.readList(this.tasbihEvents, (TasbihEvent.class.getClassLoader()));
    }

    public TasbihModel() {
    }

    public List<TasbihEvent> getTasbihEvents() {
        return tasbihEvents;
    }

    public void setTasbihEvents(List<TasbihEvent> tasbihEvents) {
        this.tasbihEvents = tasbihEvents;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(tasbihEvents);
    }

    public int describeContents() {
        return 0;
    }

}