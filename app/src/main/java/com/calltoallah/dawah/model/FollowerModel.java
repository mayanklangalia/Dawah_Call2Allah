package com.calltoallah.dawah.model;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowerModel implements Parcelable
{

    @SerializedName("user_list")
    @Expose
    private List<FollowerListModel> userList = null;
    public final static Parcelable.Creator<FollowerModel> CREATOR = new Creator<FollowerModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FollowerModel createFromParcel(Parcel in) {
            return new FollowerModel(in);
        }

        public FollowerModel[] newArray(int size) {
            return (new FollowerModel[size]);
        }

    }
            ;

    protected FollowerModel(Parcel in) {
        in.readList(this.userList, (FollowerListModel.class.getClassLoader()));
    }

    public FollowerModel() {
    }

    public List<FollowerListModel> getUserList() {
        return userList;
    }

    public void setUserList(List<FollowerListModel> userList) {
        this.userList = userList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(userList);
    }

    public int describeContents() {
        return 0;
    }

}