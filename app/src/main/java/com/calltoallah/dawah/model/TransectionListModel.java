package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransectionListModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("user_added_id")
    @Expose
    private String userAddedId;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("masjid_id")
    @Expose
    private String masjidId;
    @SerializedName("created_on")
    @Expose
    private String created_on;
    @SerializedName("amount_status")
    @Expose
    private String amountStatus;
    @SerializedName("masjid_name")
    @Expose
    private Object masjidName;
    public final static Parcelable.Creator<TransectionListModel> CREATOR = new Creator<TransectionListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public TransectionListModel createFromParcel(Parcel in) {
            return new TransectionListModel(in);
        }

        public TransectionListModel[] newArray(int size) {
            return (new TransectionListModel[size]);
        }

    };

    protected TransectionListModel(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.postId = ((String) in.readValue((String.class.getClassLoader())));
        this.userAddedId = ((String) in.readValue((String.class.getClassLoader())));
        this.amount = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidId = ((String) in.readValue((String.class.getClassLoader())));
        this.created_on = ((String) in.readValue((String.class.getClassLoader())));
        this.amountStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidName = ((Object) in.readValue((Object.class.getClassLoader())));
    }

    public TransectionListModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUserAddedId() {
        return userAddedId;
    }

    public void setUserAddedId(String userAddedId) {
        this.userAddedId = userAddedId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMasjidId() {
        return masjidId;
    }

    public void setMasjidId(String masjidId) {
        this.masjidId = masjidId;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String datetime) {
        this.created_on = created_on;
    }

    public String getAmountStatus() {
        return amountStatus;
    }

    public void setAmountStatus(String amountStatus) {
        this.amountStatus = amountStatus;
    }

    public Object getMasjidName() {
        return masjidName;
    }

    public void setMasjidName(Object masjidName) {
        this.masjidName = masjidName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(postId);
        dest.writeValue(userAddedId);
        dest.writeValue(amount);
        dest.writeValue(masjidId);
        dest.writeValue(created_on);
        dest.writeValue(amountStatus);
        dest.writeValue(masjidName);
    }

    public int describeContents() {
        return 0;
    }
}