package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SalaahModel implements Parcelable {

    @SerializedName("salaah_events")
    @Expose
    private List<SalaahEvent> salaahEvents = null;
    public final static Creator<SalaahModel> CREATOR = new Creator<SalaahModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SalaahModel createFromParcel(Parcel in) {
            return new SalaahModel(in);
        }

        public SalaahModel[] newArray(int size) {
            return (new SalaahModel[size]);
        }

    };

    protected SalaahModel(Parcel in) {
        in.readList(this.salaahEvents, (SalaahEvent.class.getClassLoader()));
    }

    public SalaahModel() {
    }

    public List<SalaahEvent> getSalaahEvents() {
        return salaahEvents;
    }

    public void setSalaahEvents(List<SalaahEvent> salaahEvents) {
        this.salaahEvents = salaahEvents;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(salaahEvents);
    }

    public int describeContents() {
        return 0;
    }

}