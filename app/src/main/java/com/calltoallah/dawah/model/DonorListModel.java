package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DonorListModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("wallet_total")
    @Expose
    private String walletTotal;
    @SerializedName("donation_amount")
    @Expose
    private String donationAmount;
    @SerializedName("user_avatar")
    @Expose
    private String user_avatar;

    public final static Parcelable.Creator<DonorListModel> CREATOR = new Creator<DonorListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DonorListModel createFromParcel(Parcel in) {
            return new DonorListModel(in);
        }

        public DonorListModel[] newArray(int size) {
            return (new DonorListModel[size]);
        }

    };

    protected DonorListModel(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.fullName = ((String) in.readValue((String.class.getClassLoader())));
        this.phoneNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.walletTotal = ((String) in.readValue((String.class.getClassLoader())));
        this.donationAmount = ((String) in.readValue((String.class.getClassLoader())));
        this.user_avatar = ((String) in.readValue((String.class.getClassLoader())));
    }

    public DonorListModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWalletTotal() {
        return walletTotal;
    }

    public void setWalletTotal(String walletTotal) {
        this.walletTotal = walletTotal;
    }

    public String getDonationAmount() {
        return donationAmount;
    }

    public void setDonationAmount(String donationAmount) {
        this.donationAmount = donationAmount;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(fullName);
        dest.writeValue(phoneNumber);
        dest.writeValue(email);
        dest.writeValue(address);
        dest.writeValue(username);
        dest.writeValue(walletTotal);
        dest.writeValue(donationAmount);
        dest.writeValue(user_avatar);
    }

    public int describeContents() {
        return 0;
    }
}