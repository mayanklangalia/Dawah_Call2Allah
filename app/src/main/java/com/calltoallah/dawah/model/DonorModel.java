package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DonorModel implements Parcelable {

    @SerializedName("top_donar")
    @Expose
    private List<DonorListModel> topDonar = null;
    public final static Parcelable.Creator<DonorModel> CREATOR = new Creator<DonorModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DonorModel createFromParcel(Parcel in) {
            return new DonorModel(in);
        }

        public DonorModel[] newArray(int size) {
            return (new DonorModel[size]);
        }

    };

    protected DonorModel(Parcel in) {
        in.readList(this.topDonar, (DonorListModel.class.getClassLoader()));
    }

    public DonorModel() {
    }

    public List<DonorListModel> getTopDonar() {
        return topDonar;
    }

    public void setTopDonar(List<DonorListModel> topDonar) {
        this.topDonar = topDonar;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(topDonar);
    }

    public int describeContents() {
        return 0;
    }

}